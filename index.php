﻿<?php
	session_start();

	//checking if someone is already logged in
	if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true)
	{
		//head to home page without doing the rest of the code
		header('Location: pages/home.php');
		exit(); 
	}
?>

<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "styles/root.css">
	<link rel = "stylesheet" href = "styles/common.css">
	<link rel = "stylesheet" href = "styles/index.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
</head>

<body>

	<header>
		<h1>No Story</h1>
		<h2>Only the dead saw the end of this project ...</h2><br><br>
	</header>

	<main>
	
		<form class = "logOn" action = "php/login.php" method = "post">

			LOGIN: <div class = "userInput"><input type = "text" name = "login"><br/></div>

			PASSWORD: <div class = "userInput"><input type = "password" name = "password"><br/></div>
			
			<input type = "submit" value = "LOGIN" class = "baseBtn indBtn"/>
					
			<div class = "errorMsg">
				<?php 
					if(isset($_SESSION['login_error']))echo $_SESSION['login_error']; 
				?>
			</div>

			<div class = "changelog">
				Update content - up to 100 lvl. Major navigation changes, make sure to clean cache for better experience.
				<br/>
				Incoming new adventures: new maps, new items tiers, skilled monsters and much more...
			</div>

		</form>
		
		<article id = "regMenu">
			<br/><a href = "pages/register.php"><div class = "baseBtn indBtn">REGISTER</div></a><br/><br/>
		</article>
		
		
	</main>
	
	<br/><br/>

	<footer>
		&copy;2019 Mateusz Fijak  / mateusz.michal.fijak@gmail.com
	</footer>
	
	<?php 
		if(isset($_SESSION['login_error'])) unset($_SESSION['login_error']); 
	?>
	
</body>

</html>