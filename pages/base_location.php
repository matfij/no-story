<?php

	include '../php/Initializer.php';
	include '../php/Rival.php';
	include '../php/CombatCalculator.php';

	session_start();
	
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../index.php');
		exit();
	}
	
?>


<!DOCTYPE html>

<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link rel = "stylesheet" href = "../styles/base_location.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
</head>


<?php  

	$player = new Player();
	$player = $_SESSION['player'];

	$skill_tree = new SkillTree();
	$skill_tree = $_SESSION['skill_tree'];

	$player_hunter = $player->skill_tree[14] * $skill_tree->skills[14][2];


	if(isset($_POST['location_id']))  
		$location = $_POST['location_id'];
	else 
		$location = $_SESSION['global_loc'];
	
	$_SESSION['global_loc'] = $location;

	//connection setup
	ini_set("display_errors", 0);
	require_once "../php/db_data.php";
	$connection = new mysqli($host, $db_user, $db_password, $db_name);
				
	mysqli_query($connection, "SET CHARSET utf8");
	mysqli_query($connection, "SET NAMES 'utf8' COLLATE 'utf8_polish_ci'");
	mysqli_select_db($connection, $database);
						
	//choosing opponent
	if($_POST['location_id'] != 0)
	{
		$hardness = $player->level % 10;

		if($player->level >= 10*$_POST['location_id'])	
			$hardness = 100;
		else if($player->level+10 < 10*$_POST['location_id'])	
			$hardness = 1;
		
		if($hardness < 3)	     $hardness = rand(1,275);
		else if($hardness < 6)	 $hardness = rand(55,600);
		else if($hardness < 10)	 $hardness = rand(255,805+$player_hunter);
		else	                 $hardness = rand(355,810+$player_hunter);
	}
	else
		$hardness = 0;
	
	$rival = new Rival();
	$rival->generateMonster($connection, $_SESSION['global_loc'], $hardness);

	$_SESSION['rival'] = $rival;
?>

<body>


	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top customNav">

	  <a class="navbar-brand" href="../pages/home.php"> <b>No Story </b></a>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">

		  <li class="nav-item">
			<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="character.php">Character</a>
		  </li>
		  <li class="nav-item active">
				<a class="nav-link" href="explore.php">Explore</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="shop.php">Shop</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="forum.php">Forum</a>
		  </li>

		</ul>
	  </div>
	</nav>

	<br/><br/>

	<!--////////////////////////////////// UPPER SECITON /////////////////////////////////////////-->
	<header>

		<?php

		echo "<br/><h2>Level: ".$player->level." | Stamina: ".$player->stamina.'</h2>';
		echo "<h4>Gold: ".$player->gold." | Unique shards: ".$player->unique_shards." | Legendary shards: ".$player->legendary_shards.'</h4>';


		$my_query = "SELECT * FROM levels WHERE lvl = ".($player->level+1);	
		$sql_answer = mysqli_query($connection, $my_query);
		$row = mysqli_fetch_assoc($sql_answer);
		$goal = $row['exp'];

		$my_query = "SELECT * FROM levels WHERE lvl = ".$player->level;	
		$sql_answer = mysqli_query($connection, $my_query);
		$row = mysqli_fetch_assoc($sql_answer);
		$start = $row['exp'];

		echo '<div class = "levelInfo"><h1>
				<progress class = "expBar" value="'.($player->experience-$start).'" max="'.($goal-$start).'"></progress></h1>
				<div class = "levelLabel">Progress:<br/>'.$player->experience.' / '.$goal.'</div>
			 </div>';

		?>

		<div class = "separator"></div>

	</header>
	
	
	<main>
	
		<div class = "row">
		
			<!--////////////////////////////////// PLAYER SECITON /////////////////////////////////////////-->
			<div class = "col-md-12  col-lg-3 combatant">  
				<?php	

					echo '</br><h3>'.$player->nick.'</h4>';

					if($player->avatar != 'empty')  
						echo $player->avatar;
					else  
						echo '<img class = "avatar" src = "../img/avatars/noname.png">';	
						
					echo '</br>Health: '.$player->full_health;
					echo '</br>Attack: '.$player->full_attack;
					echo '</br>Damage: '.$player->full_damage;
					echo '</br>Agility: '.$player->full_agility;
					echo '</br>Armor: '.$player->full_armor;
				?>
			</div>
			
			
			<!--////////////////////////////////// COMBAT SECTION SECITON /////////////////////////////////////////-->
			<div class = "col-md-12  col-lg-6 combatWindow" id = "cWin">
				<?php	

					$combatCalculator = new CombatCalculator();

					$combatCalculator->executeCombat($player, $rival, 1);

					$combatCalculator->saveResults($connection, $player, $rival);
		
				?>
			</div>
			

			<!--////////////////////////////////// ENEMY SECITON /////////////////////////////////////////-->
			<div class = "col-md-12  col-lg-3 combatant">
				<?php

					$e_img = 'l'.$_SESSION['global_loc'].'m'.$rival->id;

					echo '</br><h3>'.$rival->nick.'</h4>';
					echo '<img class = "avatar" src = "../img/enemies/'.$e_img.'.jpg">';
					echo '</br>Health: '.$rival->full_health;
					echo '</br>Attack: '.$rival->full_attack;
					echo '</br>Damage: '.$rival->full_damage;
					echo '</br>Agility: '.$rival->full_agility;
					echo '</br>Armor: '.$rival->full_armor;
				?>
			</div>
	
		</div>
	
        
	
		<article id = "navMenu">

			<form action = "base_location.php" method = "post">
				<?php    
					echo '<div class = "hiddenInput"><input type = "text" name = "location_id" value="'.$_POST['location_id'].'"></div>';
					echo '<input type = "submit" value = "CONTINUE" class = "baseBtn"/>';
				?>
			</form>
			
			<a href = "explore.php"><div class = "baseBtn inLocBtn">BACK</div></a><br/>

		</article>
		
	</main>

	<script type="text/javascript" src="../js/combatScroll.js"></script>
	<script src="../static/js/jqmin.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>

</body>
</html>