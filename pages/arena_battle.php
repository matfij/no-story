<?php

	include '../php/Initializer.php';
	include '../php/Rival.php';

	session_start();
	
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../index.php');
		exit();
	}


	if(isset($_POST['rival_id']))
	{
		$rival_id = $_POST['rival_id'];

		require_once "../php/db_data.php";
		$connection = @new mysqli($host, $db_user, $db_password, $db_name);	
		mysqli_query($connection, "SET CHARSET utf8");
		mysqli_query($connection, "SET NAMES 'utf8' COLLATE 'utf8_polish_ci'");

		//initialize rival
		$init_object = new Initializer;
		$init_object -> initializeRival($connection, '', $rival_id);

		$rival = new Rival();
		$rival = $_SESSION['rival'];

		$connection->close();
	}
    else if(isset($_SESSION['rival']))
	{
		$rival = new Rival();
		$rival = $_SESSION['rival'];
	}
	else
	{
		header('Location: ../index.php');
		exit();
	}


	$player = new Player();
	$player = $_SESSION['player'];

?>


<!DOCTYPE html>

<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link rel = "stylesheet" href = "../styles/base_location.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">

	<script type="text/javascript" src="../js/displayCombat.js"></script>
	<script type="text/javascript" src="../js/jq.js"></script>
	
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top customNav">

	  <a class="navbar-brand" href="../pages/home.php"> <b>No Story </b></a>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">

		  <li class="nav-item">
			<a class="nav-link" href="home.php"> Home</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="character.php">Character</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="explore.php">Explore</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="shop.php">Shop</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="forum.php">Forum</a>
		  </li>

		</ul>
	  </div>
	</nav>

	<br/><br/><br/>

	<header>
		<h1>vs <?php echo $rival->nick; ?>!</h1>
		<h4><br/>Combat begins:</h4><br/>
	</header>
	
	<main>
	<div class = "row">

		<!--////////////////////////////////// PLAYER SECITON /////////////////////////////////////////-->
		<div class = "col-md-12  col-lg-3 combatant">  
			<?php	

				echo '</br><h3>'.$player->nick.'</h4>';
						
				if($player->avatar != 'empty')  
					echo $player->avatar;
				else  
					echo '<img class = "avatar" src = "../img/avatars/noname.png">';
						
				echo '</br>Health: '.$player->full_health;
				echo '</br>Attack: '.$player->full_attack;
				echo '</br>Damage: '.$player->full_damage;
				echo '</br>Agility: '.$player->full_agility;
				echo '</br>Armor: '.$player->full_armor;
			?>
		</div>


		<!--////////////////////////////////// COMBAT SECTION SECITON /////////////////////////////////////////-->
		<div class = "col-md-12  col-lg-6 combatWindow" id = "cWin">
			
			<script> displayCombat(600); </script>

		</div>


		<!--////////////////////////////////// RIVAL SECITON /////////////////////////////////////////-->
		<div class = "col-md-12  col-lg-3 combatant">  
			<?php	

				echo '</br><h3>'.$rival->nick.'</h4>';

				if($rival->avatar != 'empty')  
					echo $rival->avatar;
				else  
					echo '<img class = "avatar" src = "../img/avatars/noname.png">';	

				echo '</br>Health: '.$rival->full_health;
				echo '</br>Attack: '.$rival->full_attack;
				echo '</br>Damage: '.$rival->full_damage;
				echo '</br>Agility: '.$rival->full_agility;
				echo '</br>Armor: '.$rival->full_armor;
			?>
		</div>

	</div>

		<article id = "navMenu">
			<br><br>
			<a href = "../pages/arena_battle.php"><div class = "baseBtn">RETRY</div></a>
			<br>
			<a href = "../pages/arena.php"><div class = "baseBtn">BACK</div></a>
			<br/>
		</article>
		
	</main>

	<script src="../static/js/jqmin.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>

</body>
</html>