<?php
	session_start();
	
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../index.php');
		exit();
	}
?>


<!DOCTYPE html>

<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link rel = "stylesheet" href = "../styles/ranking.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top customNav">

	  <a class="navbar-brand" href="../pages/home.php"> <b>No Story </b></a>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">

		  <li class="nav-item">
			<a class="nav-link" href="home.php"> Home</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="character.php">Character</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="explore.php">Explore</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="shop.php">Shop</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="forum.php">Forum</a>
		  </li>

		</ul>
	  </div>
	</nav>

	<br/><br/>

	<header>
		<h1>Hall of Fame</h1>
	</header>
	
	<main>
	
	    <table class="rankingTable"><tr>

        <?php
		  require_once "../php/db_data.php";
            $my_connection = @new mysqli($host, $db_user, $db_password, $db_name);
	
			mysqli_query($my_connection, "SET CHARSET utf8");
            
            $my_query = 'SELECT * FROM players ORDER BY exp DESC';
            
            $my_answer = mysqli_query($my_connection, $my_query);
			
            $player_no = mysqli_num_rows($my_answer);

if ($player_no>=1) 
{
echo<<<END
<td><b>Nickname</b></td>
<td><b>Level</b></td>
<td><b>Experience</b></td>

</tr><tr>
END;
}

	for ($i = 1; $i <= $player_no; $i++) 
	{
	
		$row = mysqli_fetch_assoc($my_answer);
		$a1 = $row['NICK'];
		$a2 = $row['LVL'];
		$a3 = $row['EXP'];	
		
echo<<<END
<td>$a1</td>
<td>$a2</td>
<td>$a3</td>
</tr><tr>
END;
			
	}
	$my_connection->close();
?>

</tr></table>
	
		<article id = "navMenu">
			<br><br>
			<a href = "home.php"><div class = "baseBtn">BACK</div></a>
			<br/>
		</article>
		
	</main>
	
		<script src="../static/js/jqmin.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>

</body>
</html>