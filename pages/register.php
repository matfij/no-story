<?php
	session_start();
	
	if(isset($_POST['newNick']))
	{
		$all_ok = true;
		
		//VALIDATION CHECK:
		
		$new_nick = $_POST['newNick'];
		
		if(ctype_alnum($new_nick) == false)
		{
			$all_ok = false;
			$_SESSION['err_nick'] = "nick contains illegal characters";	
		} 
		if(strlen($new_nick) < 3 || strlen($new_nick) > 12)
		{
			$all_ok = false;
			$_SESSION['err_nick'] = "nick must contain 3 to 12 characters";	
		}
		
		
		$new_email = $_POST['newEmail'];
		$safe_mail = filter_var($new_email, FILTER_SANITIZE_EMAIL);
		
		if(filter_var($safe_mail, FILTER_VALIDATE_EMAIL) == false || $new_email != $safe_mail)
		{
			$all_ok = false;
			$_SESSION['err_email'] = "invalid email address";
		}
		
		$new_pass1 = $_POST['newPass1'];
		$new_pass2 = $_POST['newPass2'];
		
		if(strlen($new_pass1)<8 || strlen($new_pass1)>20)
		{
			$all_ok = false;
			$_SESSION['err_pass'] = "password must contain 8 to 20 characters";
		}
		if($new_pass1 != $new_pass2)
		{
			$all_ok = false;
			$_SESSION['err_pass'] = "passwords do not match";
		}

		//hashing password
		$hashed_pass = password_hash($new_pass1, PASSWORD_DEFAULT);

		if(!isset($_POST['checkBox']))
		{
			$all_ok = false;
			$_SESSION['err_check'] = "you must accept our rules";
		}
		
		////////////////////	CAPTCHA	   //////////////////////
		/* $secret_key = '6LcxIYIUAAAAANMzNHeUy-wZpl8zr09xSCjVqi0t';
		
		$check = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$_POST['g-recaptcha-response']);
		$google_answer = json_decode($check);
		
		if($google_answer->success != true)
		{
			$all_ok = false;
			$_SESSION['err_bot'] = "you are a bot";
		} */
		
		
		//////////////////	CONNECTING TO DATABASE	/////////////////
		require_once "../php/db_data.php";
		
		mysqli_report(MYSQLI_REPORT_STRICT);
		
		try
		{
			$my_connection = new mysqli($host, $db_user, $db_password, $db_name);
			
			if($my_connection->connect_errno!=0)  // NO connection
			{
				throw new Exception(mysqli_connect_errno());
			}
			else
			{
				// CHECK IF EMAIL IS UNIQUE
				$sqli_answer = $my_connection->query("SELECT PID FROM players WHERE MAIL ='$new_email'");				
				if(!$sqli_answer) throw new Exception($my_connection->error);
				
				$mail_cnt = $sqli_answer->num_rows;
				if($mail_cnt > 0)
				{
					$all_ok = false;
					$_SESSION['err_email'] = "this email address is already occupated";
				}
				
				// CHECK IF LOGIN IS UNIQUE
				$sqli_answer = $my_connection->query("SELECT PID FROM players WHERE NICK ='$new_nick'");				
				if(!$sqli_answer) throw new Exception($my_connection->error);
				
				$nick_cnt = $sqli_answer->num_rows;
				if($nick_cnt > 0)
				{
					$all_ok = false;
					$_SESSION['err_email'] = "this nick is already occupated";
				}
				
				//		ALL 	OK
				if($all_ok == true)
				{
					//getting latest ID
					$sqli_answer = $my_connection->query("SELECT PID from players ORDER BY PID DESC LIMIT 1");
					$decoded_answer = $sqli_answer->fetch_assoc();
					$next_id = 1 + $decoded_answer['PID'];
					
					$my_query1 = 'INSERT INTO players (PID, NICK, PASS, MAIL, AVIMG) VALUES
					('.$next_id.', "'.$new_nick.'", "'.$hashed_pass.'", "'.$safe_mail.'", 0)';	
					
					$my_query2 = 'insert into skilltrees (pid) values ('.$next_id.')';
					
					if(mysqli_query($my_connection, $my_query1) && mysqli_query($my_connection, $my_query2))
					{
						$_SESSION['register_completed'] = true;
						header('Location: welcome.php');
					}
					else
					{
						throw new Exception($my_connection->error);
					}
				}
				
				$my_connection->close();
			}
		}
		catch(Exception $ex)
		{
			echo '<span>server error 2</span>';
			echo ' : '.$ex;	//FULL ERROR INFO	
			exit();
		}
	}
?>


<!DOCTYPE html>

<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link rel = "stylesheet" href = "../styles/register.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>

	<header>
		<h2>Create new account:</h2><br>
	</header>

	<main>
	
		<form class = "registerContainer" method = "post">
			NICKNAME:<br/><input type = "text" name = "newNick">
			<div class = "errorMsg">
				<?php
					if(isset($_SESSION['err_nick']))
					{
						echo $_SESSION['err_nick'];
						unset($_SESSION['err_nick']);
					}
				?>
			</div>
			
			<br/>EMAIL:<br/><input type = "text" name = "newEmail">
			<div class = "errorMsg">
				<?php
					if(isset($_SESSION['err_email']))
					{
						echo $_SESSION['err_email'];
						unset($_SESSION['err_email']);
					}
				?>
			</div>
			
			<br/>PASSWORD:<br/><input type = "password" name = "newPass1">
			<div class = "errorMsg">
				<?php
					if(isset($_SESSION['err_pass']))
					{
						echo $_SESSION['err_pass'];
						unset($_SESSION['err_pass']);
					}
				?>
			</div>			
			<br/>RE-ENTER PASSWORD:<br/><input type = "password" name = "newPass2">
			<div class = "errorMsg">
				<?php
					if(isset($_SESSION['err_pass2']))
					{
						echo $_SESSION['err_pass'];
						unset($_SESSION['err_pass']);
					}
				?>
			</div>	
			
			<br/><br/><input type = "checkbox" name = "checkBox"> I agree to the <a>Terms of Use and Privacy Policy</a><br/>
			<div class = "errorMsg">
				<?php
					if(isset($_SESSION['err_check']))
					{
						echo $_SESSION['err_check'];
						unset($_SESSION['err_check']);
					}
				?>	
			</div>
			
			<!-- <br/><div class="g-recaptcha" data-sitekey="6LcxIYIUAAAAAAkGEImpDNvELaABcZqw7sWD9A4Y"></div> -->

			<br/><div class = "errorMsg">
				<?php
					if(isset($_SESSION['err_bot']))
					{
						echo $_SESSION['err_bot'];
						unset($_SESSION['err_bot']);
					}
				?>	
			</div>
				
			<br/><input type = "submit" value = "REGISTER" class = "baseBtn"/>
			
		</form>
		

		<article id = "navMenu">
			<br/><a href = "home.php"><div class = "baseBtn">BACK</div></a><br/>
		</article>
		
	</main>
	

	
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	
</body>
</html>