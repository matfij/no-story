﻿<?php
	session_start();
	
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../index.php');
		exit();
	}
?>

<html lang = "pl">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link rel = "stylesheet" href = "../styles/forum.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top customNav">

	  <a class="navbar-brand" href="../pages/home.php"> <b>No Story </b></a>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">

		  <li class="nav-item">
			<a class="nav-link" href="home.php"> Home</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="character.php">Character</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="explore.php">Explore</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="shop.php">Shop</a>
		  </li>
		  <li class="nav-item active">
			<a class="nav-link" href="#">Forum</a>
		  </li>

		</ul>
	  </div>
	</nav>

	<br/><br/>

	<header>
		<h1>Latest Posts:</h1><br>
	</header>
	
	<main>

		<div class = "errorMessageWrapper row">
			<?php

				if(isset($_SESSION['post_error']))
				{
					echo '<div class = "errorMsg col-md-12">'.$_SESSION['forum_message'].'<br/><br/></div>';
				}

				unset($_SESSION['post_error']);

			?>
		</div>
	

		<article id = "userPosts">
		<?php

			//connecting to database
			require_once "../php/db_data.php";
			$my_connection = @new mysqli($host, $db_user, $db_password, $db_name);

			//setting encoding standard
			mysqli_query($my_connection, "SET CHARSET utf8");

			//querry
			$my_querry = "select * from posts order by id desc";
			$my_answer = mysqli_query($my_connection, $my_querry);
			$post_no = mysqli_num_rows($my_answer);

			//displaying posts
			for($cnt = 1; $cnt <= $post_no; $cnt++)
			{
				$row = mysqli_fetch_assoc($my_answer);

				//fetching author name
				$my_querry = "select NICK from players where PID =".$row['PID'];
				$author = mysqli_fetch_assoc(mysqli_query($my_connection, $my_querry));

				$message = $row['CONTENT'];

				echo '<br/><div class = "playerPost"><h3>'.$author['NICK'].'</h3>
				<div class = "messageContainer">'.$message.'<br/><br/></div>
				</div>';
			}
			echo '<br/>';
		?>
		</article>

		<article id = "newPost">
		<form action = "../php/add_post.php" method = "post">

			<br/><h3>ADD A NEW POST:</h3>

			<textarea class="postInput" type = "textarea" name = "message"></textarea><br/><br/>
			
			<input type = "submit" value = "POST" class = "baseBtn"/>

		</form>
		</article>
		
		<article id = "navMenu">
			<br/><a href = "home.php"><div class = "baseBtn">BACK</div></a><br/>
		</article>
		
	</main>
	
	<script src="../static/js/jqmin.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>

</body>
</html>