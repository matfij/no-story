<?php
	session_start();
	//checking if register ok
	if(!isset($_SESSION['register_completed']))
	{
		//head to home page without doing the rest of the code
		header('Location: ../index.php');
		exit(); 
	}
	else
	{
		unset($_SESSION['register_completed']);
	}
?>


<!DOCTYPE html>

<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
</head>

<body>

	<main>
	
		<br/>
		<h2>Registred succesfully!
		<br/>
		You are ready to play now.
		<br/><br/>
		

	<article id = "navMenu">
		<a href = "../index.php"><div class = "baseBtn">START</div></a>
	</article>
		
	</main>
	
</body>
</html>