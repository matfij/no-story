<?php
	session_start();
	
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../index.php');
		exit();
	}
?>


<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link rel = "stylesheet" href = "../styles/explore.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
</head>


<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top customNav">

	  <a class="navbar-brand" href="../pages/home.php"> <b>No Story </b></a>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">

		  <li class="nav-item">
			<a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="character.php">Character</a>
		  </li>
		  <li class="nav-item active">
				<a class="nav-link" href="#">Explore</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="shop.php">Shop</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="forum.php">Forum</a>
		  </li>

		</ul>
	  </div>
	</nav>

	<br/><br/><br/>

	<main>
	
		<article id = "locationMenu">
		
			<div class = "landWrapper">
			<h2>Old Continent</h2>

			<form action = "base_location.php" method = "post">
				<?php    
						echo '<div class = "hiddenInput"><input type = "text" name = "location_id" value="1"> <br/></div>';
						echo '<input type = "submit" value = "WHITE TUNNEL" class = "baseBtn"/><br/>';
						echo 'Suggested Level: 1 <br/><br/>';
				?>
			</form>
			
			<form action = 'base_location.php' method = "post">
				<?php    
						echo '<div class = "hiddenInput"><input type = "text" name = "location_id" value="2"> <br/></div>';
						echo '<input type = "submit" value = "DRAGON VAULT" class = "baseBtn"/><br/>';
						echo 'Suggested Level: 10 <br/><br/>';
				?>
			</form>
			
			<form action = 'base_location.php' method = "post">
				<?php    
						echo '<div class = "hiddenInput"><input type = "text" name = "location_id" value="3"> <br/></div>';
						echo '<input type = "submit" value = "SHADY FOREST" class = "baseBtn"/><br/>';
						echo 'Suggested Level: 20 <br/><br/>';
				?>
			</form>
			
			<form action = 'base_location.php' method = "post">
				<?php    
						echo '<div class = "hiddenInput"><input type = "text" name = "location_id" value="4"> <br/></div>';
						echo '<input type = "submit" value = "GRAND CANYON" class = "baseBtn"/><br/>';
						echo 'Suggested Level: 30 <br/><br/>';
				?>
			</form>
			
			<form action = 'base_location.php' method = "post">
				<?php    
						echo '<div class = "hiddenInput"><input type = "text" name = "location_id" value="5"> <br/></div>';
						echo '<input type = "submit" value = "ANCIENT TEMPLE" class = "baseBtn"/><br/>';
						echo 'Suggested Level: 40 <br/><br/>';
				?>
			</form>
			
			<form action = 'base_location.php' method = "post">
				<?php    
						echo '<div class = "hiddenInput"><input type = "text" name = "location_id" value="6"> <br/></div>';
						echo '<input type = "submit" value = "PIRATE PORT" class = "baseBtn"/><br/>';
						echo 'Suggested Level: 50 <br/><br/>';
				?>
			</form>

			</div>

			<br/>

			<div class = "landWrapper">
			<h2>Dark Sea</h2>

			<form action = 'base_location.php' method = "post">
				<?php    
						echo '<div class = "hiddenInput"><input type = "text" name = "location_id" value="7"> <br/></div>';
						echo '<input type = "submit" value = "RESTLESS WATER" class = "baseBtn"/><br/>';
						echo 'Suggested Level: 60 <br/><br/>';
				?>
			</form>
			
			<form action = 'base_location.php' method = "post">
				<?php    
						echo '<div class = "hiddenInput"><input type = "text" name = "location_id" value="8"> <br/></div>';
						echo '<input type = "submit" value = "GIANT ICEBERG" class = "baseBtn"/><br/>';
						echo 'Suggested Level: 70 <br/><br/>';
				?>
			</form>

			<form action = 'base_location.php' method = "post">
				<?php    
						echo '<div class = "hiddenInput"><input type = "text" name = "location_id" value="9"> <br/></div>';
						echo '<input type = "submit" value = "SULFUR BAY" class = "baseBtn"/><br/>';
						echo 'Suggested Level: 80 <br/><br/>';
				?>
			</form>

			<form action = 'base_location.php' method = "post">
				<?php    
						echo '<div class = "hiddenInput"><input type = "text" name = "location_id" value="10"> <br/></div>';
						echo '<input type = "submit" value = "PESTIFEROUS PIT" class = "baseBtn"/><br/>';
						echo 'Suggested Level: 90 <br/><br/>';
				?>
			</form>



			</div>


			<br/>
			<a href = "home.php"><div class = "baseBtn">BACK</div></a><br/>
		</article>
		
	</main>

	<script src="../static/js/jqmin.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
	
	
</body>
</html>