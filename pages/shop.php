<?php

	include '../php/Player.php';
	//include '../php/ShopDisplay.php';

	session_start();
	
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../index.php');
		exit();
	}

?>


<!DOCTYPE html>

<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link rel = "stylesheet" href = "../styles/shop.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top customNav">

	  <a class="navbar-brand" href="../pages/home.php"> <b>No Story </b></a>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">

		  <li class="nav-item">
			<a class="nav-link" href="home.php"> Home</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="character.php">Character</a>
		  </li>
		  <li class="nav-item">
				<a class="nav-link" href="explore.php">Explore</a>
		  </li>
		  <li class="nav-item active">
			<a class="nav-link" href="#">Shop</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="forum.php">Forum</a>
		  </li>

		</ul>
	  </div>
	</nav>

	<br/><br/>

	<header>
		<?php

			$player = new Player();
			$player = $_SESSION['player'];

			echo '<br/><h3><b>Gold: '.$player->gold.'</b></h3>';
			echo '<h4><span style="color:#0080ff;">Unique shards: '.$player->unique_shards.'</span> | 
				<span style="color:#ffae19; font-weight:900;"> Legendary shards: '.$player->legendary_shards.'</h4>';
		?>
	</header>
	
	<main>
	
		<div class = "shopMessageWrapper row">
			<?php
			if($_SESSION['purchase_completed'])
			{
				echo '<div class = "shopMessage col-md-12">'.$_SESSION['shop_message'].'</div>';
			}
			$_SESSION['purchase_completed'] = false;
			?>
		</div>

		<br/>
		<form class = "findItemsForm" id = "searchingData">
		
			LEVEL RANGE:
			<input class = "lvlInput" type="number" pattern="[0-9]" name = "lowLvl" id = "lowLvl"> - 
			<input class = "lvlInput" type="number" pattern="[0-9]" name = "highLvl" id = "highLvl">
	
			<br/>RARITY:
			<select name="rarity">
				<option value="0"> All</option>
				<option value="1"> Common</option>
				<option value="2"> Upgraded</option>
				<option value="3"> Unique</option>
				<option value="4"> Legendary</option>
			</select>

			<br/> TYPE:
			<select name="type">
				<option value="1"> Helmets </option>
				<option value="2"> Weapons </option>
				<option value="3"> Armors </option>
				<option value="4"> Shields </option>
				<option value="5"> Boots </option>
			</select>

			<br/><input type = "submit" value = "SEARCH" class = "baseBtn formBtn" id = "searchBtn" onclick = "return findItems()"/>

		</form>

	
		<section id = "itemShop">
			<div class = "row" id = "itemsContainer">
			
				<!--  HERE  DISPLAY  DESIRED  ITEMS  -->

				<div class = "col-lg-1 col-lg-offset-1"></div>

		
				
			</div>
		</section>	
		
	</main>
	
	<article id = "navMenu">
		<br/><br/><br/><br/><a href = "home.php"><div class = "baseBtn">BACK</div></a><br/>
	</article>

	<script src="../static/js/jqmin.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/displayShop.js"></script>

</body>
</html>