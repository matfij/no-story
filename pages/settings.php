<?php

	include "../php/Player.php";

	session_start();
	
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../index.php');
		exit();
	}
?>


<!DOCTYPE html>

<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link rel = "stylesheet" href = "../styles/settings.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top customNav">

	  <a class="navbar-brand" href="../pages/home.php"> <b>No Story </b></a>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">

		  <li class="nav-item">
			<a class="nav-link" href="home.php"> Home</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="character.php">Character</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="explore.php">Explore</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="shop.php">Shop</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="forum.php">Forum</a>
		  </li>

		</ul>
	  </div>
	</nav>

	<br/><br/>

	<header>
		<h1>No Story</h1>
	</header>
	
	<main>

		<?php
			$player = new Player();
			$player = $_SESSION['player'];

			echo "<h4>User: ".$player->nick;
			echo " | ID: ".$player->id;
		?>	

		<br/><br/>

		<h3><b>Avatars:</b></h3>
		1. should be 200x200 format<br/>
		2. should be a .png file<br/>
		3. should be under 50kB<br/>

		<form action="../php/upload_avatar.php" method="post" enctype="multipart/form-data">

			Select image to upload:
			<input class = "fileBtn" type="file" name="fileToUpload" id="fileToUpload" required/><br/>
			<input class = "baseBtn addAvatarBtn" type="submit" value="Upload Image" name="submit"/>
		</form>

		<div class = "settingsMessageWrapper">
			<?php
			if(isset($_SESSION['avatar_message']))
			{
				echo '<div class = "settingsMessage col-md-12">'.$_SESSION['avatar_message'].'</div>';
			}
			unset($_SESSION['avatar_message']);
			?>
		</div>

		
		<article id = "navMenu">
			<br><br>
			<a href = "home.php"><div class = "baseBtn">BACK</div></a>
			<br/>
		</article>
		
	</main>
	
	<script src="../static/js/jqmin.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>

</body>
</html>