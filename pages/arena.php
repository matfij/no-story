<?php
	session_start();
	
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../index.php');
		exit();
	}
?>


<!DOCTYPE html>

<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link rel = "stylesheet" href = "../styles/arena.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top customNav">

	  <a class="navbar-brand" href="../pages/home.php"> <b>No Story </b></a>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">

		  <li class="nav-item">
			<a class="nav-link" href="home.php"> Home</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="character.php">Character</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="explore.php">Explore</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="shop.php">Shop</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="forum.php">Forum</a>
		  </li>

		</ul>
	  </div>
	</nav>

	<br/><br/>

	<header>
		<br/><h2>Find your rival:</h2><br/>
	</header>
	
	<main>
	
	    <form class = "findPlayerForm" id = "searchingData">
		
			LEVEL RANGE:
			<input class = "lvlInput" type="number" pattern="[0-9]" name = "lowLvl" id = "lowLvl"> - 
			<input class = "lvlInput" type="number" pattern="[0-9]" name = "highLvl" id = "highLvl">
			
			<br/><input type = "submit" value = "SEARCH" class = "baseBtn" id = "searchBtn" onclick = "return findPlayers()"/>
					
		</form>

		<div class = "searchingResults">

			<br/><h3>Searching results: </h3>

			<div id = "foundPlayers">



			</div>
		</div>
	
		<article id = "navMenu">
			<br><br>
			<a href = "home.php"><div class = "baseBtn">BACK</div></a>
		</article>
		
	</main>
	
	<script type="text/javascript" src="../js/jq.js"></script>
	<script type="text/javascript" src="../js/displayPlayers.js"></script>
	<script src="../static/js/jqmin.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>

</body>
</html>