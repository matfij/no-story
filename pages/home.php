<!DOCTYPE html>
<?php

	require_once('../php/Player.php');

	session_start();
	
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../index.php');
		exit();
	}
?>




<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link rel = "stylesheet" href = "../styles/home.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top customNav">

	  <a class="navbar-brand" href="../pages/home.php"> <b>No Story </b></a>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">

		  <li class="nav-item active">
			<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="character.php">Character</a>
		  </li>
		  <li class="nav-item">
				<a class="nav-link" href="explore.php">Explore</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="shop.php">Shop</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="forum.php">Forum</a>
		  </li>

		</ul>
	  </div>
	</nav>
	
	<main>

		<br/><br/>
		<?php
			$player = new Player();
			$player = $_SESSION['player'];

			echo "<h4>User: ".$player->nick;
			echo " | Level: ".$player->level;
			echo " | Gold: ".$player->gold;
			echo " | Stamina: ".$player->stamina." | ";

		?>	

		<a href = "../php/logout.php" value = "LOGOUT" id = "logoutBtn">LOGOUT</a></h4>

		
		<article id = "navMenu">
			<br><a href = "character.php"><div class = "baseBtn">CHARACTER</div></a>
			<br><a href = "explore.php"><div class = "baseBtn">EXPLORE</div></a>
			<br><a href = "arena.php"><div class = "baseBtn">ARENA</div></a>
			<br><a href = "shop.php"><div class = "baseBtn">SHOP</div></a>
			<br><a href = "ranking.php"><div class = "baseBtn">RANKING</div></a>
			<br><a href = "forum.php"><div class = "baseBtn">FORUM</div></a>
			<br><a href = "settings.php"><div class = "baseBtn">SETTINGS</div></a>
			<br/>
		</article>
		
	</main>

    <script src="../static/js/jqmin.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>
   

</body>
</html>