<?php

	include '../php/Player.php';

	session_start();
	
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../index.php');
		exit();
	}
?>


<!DOCTYPE html>

<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link rel = "stylesheet" href = "../styles/skills.css">

	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
</head>

<body>
	<!-- GETTING ITEMS STATS -->
	<main>

		<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top customNav">

		  <a class="navbar-brand" href="../pages/home.php"> <b>No Story </b></a>

		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText">
			<span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarText">
			<ul class="navbar-nav mr-auto">

			  <li class="nav-item">
				<a class="nav-link" href="home.php"> Home</a>
			  </li>
			  <li class="nav-item active">
				<a class="nav-link" href="character.php">Character</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="explore.php">Explore</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="shop.php">Shop</a>
			  </li>
			  <li class="nav-item ">
				<a class="nav-link" href="#">Forum</a>
			  </li>

			</ul>
		  </div>
		</nav>

		<br/><br/>

		<div class = "skillHeader">
			<h2>SKILL TREE</h2>

		<?php
			$player = new Player();
			$player = $_SESSION['player'];

			echo 'Spare skill points: <b>'.$player->spare_skill_points.'</b><br/><br/>';

			//load skill tree
		?>
		</div>

		<div class = "skillsContainer row">

			<div class = "col-md-4">
				<h2>OFFENCE</h2><br/>

				<?php

					require_once "../php/db_data.php";
					$my_connection = @new mysqli($host, $db_user, $db_password, $db_name);
					$my_query = 'SELECT * FROM skills WHERE Type=1 ORDER BY requiredLevel ASC';
            
					$sql_answer = mysqli_query($my_connection, $my_query);
			
					$skills_no = mysqli_num_rows($sql_answer);

					for($cnt = 1; $cnt <= $skills_no; $cnt++)
					{
						$row = mysqli_fetch_assoc($sql_answer); 

						echo '<div class = "skillContainer">
								<img class = "skillImg" src = "../img/skills/'.$row['ID'].'.gif"/>
								<br/>'.$player->skill_tree[$row['ID']].'/10
								';

						if($player->spare_skill_points > 0 && $player->level >= $row['RequiredLevel'] && $player->skill_tree[$row['ID']] < 10)
						{
							echo '<form class = "skillForm" action = "../php/skill_plus.php" method = "post">
								<div class = "hiddenInput"><input type = "text" name = "skill_id" value='.$row['ID'].'></div>
								<input type = "submit" value = "+" class = "skillBtn"/>
							</form>';
						}
						echo '<div class = "skillHover">'.$row['Name'].'<br/></div></div>';
					}

				?>
					
			</div>

			<div class = "col-md-4">
				<h2>SPECIAL</h2><br/>

					<?php
						$my_query = 'SELECT * FROM skills WHERE Type=2 ORDER BY requiredLevel ASC';
            
						$sql_answer = mysqli_query($my_connection, $my_query);
			
						$skills_no = mysqli_num_rows($sql_answer);

						for($cnt = 1; $cnt <= $skills_no; $cnt++)
						{
							$row = mysqli_fetch_assoc($sql_answer);

							echo '<div class = "skillContainer">
									<img class = "skillImg" src = "../img/skills/'.$row['ID'].'.gif"/>
									<br/>'.$player->skill_tree[$row['ID']].'/10
									';

							if($player->spare_skill_points > 0 && $player-> level >= $row['RequiredLevel'] && $player->skill_tree[$row['ID']] < 10)
							{
								echo '<form class = "skillForm" action = "../php/skill_plus.php" method = "post">
									<div class = "hiddenInput"><input type = "text" name = "skill_id" value='.$row['ID'].'></div>
									<input type = "submit" value = "+" class = "skillBtn"/>
								</form>';
							}
							echo '<div class = "skillHover">'.$row['Name'].'<br/></div></div>';
						}
					?>
					
			</div>

			<div class = "col-md-4">
				<h2>DEFENSE</h2><br/>

					<?php
						$my_query = 'SELECT * FROM skills WHERE Type=3 ORDER BY requiredLevel ASC';
            
						$sql_answer = mysqli_query($my_connection, $my_query);
			
						$skills_no = mysqli_num_rows($sql_answer);

						for($cnt = 1; $cnt <= $skills_no; $cnt++)
						{
							$row = mysqli_fetch_assoc($sql_answer);

							echo '<div class = "skillContainer">
									<img class = "skillImg" src = "../img/skills/'.$row['ID'].'.gif"/>
									<br/>'.$player->skill_tree[$row['ID']].'/10
									';

							if($player->spare_skill_points > 0 && $player-> level >= $row['RequiredLevel'] && $player->skill_tree[$row['ID']] < 10)
							{
								echo '<form class = "skillForm" action = "../php/skill_plus.php" method = "post">
									<div class = "hiddenInput"><input type = "text" name = "skill_id" value='.$row['ID'].'></div>
									<input type = "submit" value = "+" class = "skillBtn"/>
								</form>';
							}
							echo '<div class = "skillHover skillHoverLeft">'.$row['Name'].'<br/></div></div>';
						}
					?>
					
			</div>

		</div>

		<article id = "navMenu">
			<br><br><a href = "character.php"><div class = "baseBtn">BACK</div></a><br/>
		</article>

	</main>
	

</body>
</html>