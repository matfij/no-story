<?php

	include '../php/Player.php';

	session_start();
	
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../index.php');
		exit();
	}
?>


<!DOCTYPE html>

<html lang = "en">
<head>

	<meta charset = "utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>No Story</title>
	
	<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel = "stylesheet" href = "../styles/root.css">
	<link rel = "stylesheet" href = "../styles/common.css">
	<link rel = "stylesheet" href = "../styles/character.css">
	<link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700&amp;subset=latin-ext" rel="stylesheet">
	
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top customNav">

	  <a class="navbar-brand" href="../pages/home.php"> <b>No Story </b></a>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">

		  <li class="nav-item">
			<a class="nav-link" href="home.php"> Home</a>
		  </li>
		  <li class="nav-item active">
			<a class="nav-link" href="#">Character</a>
		  </li>
		  <li class="nav-item">
				<a class="nav-link" href="explore.php">Explore</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="shop.php">Shop</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="forum.php">Forum</a>
		  </li>

		</ul>
	  </div>
	</nav>

	<br/><br/>

	<!-- GETTING ITEMS STATS -->
	<?php
		//connecting to database
		require_once "../php/db_data.php";
		$connection = @new mysqli($host, $db_user, $db_password, $db_name);

		$player = new Player();
		$player = $_SESSION['player'];
		

		$my_query = "SELECT * FROM levels WHERE lvl = ".($player->level+1);	
		$answer_sql = mysqli_query($connection, $my_query);
		$row = mysqli_fetch_assoc($answer_sql);
		$goal = $row['exp'];

		$my_query = "SELECT * FROM levels WHERE lvl = ".$player->level;	
		$answer_sql = mysqli_query($connection, $my_query);
		$row = mysqli_fetch_assoc($answer_sql);
		$start = $row['exp'];

		echo '<br/><div class = "levelInfo"><h1>'.$player->nick.' | Level: '.$player->level.
				'<br/><progress class = "expBar" value="'.($player->experience-$start).'" max="'.($goal-$start).'"></progress></h1>
				<div class = "levelLabel profileLabel">Progrss:<br/>'.$player->experience.' / '.$goal.'</div>
			 </div>';

		$answer_sql->free();
		$connection->close();
	?>

	
	<main>
				
		<?php
			$player = new Player();
			$player = $_SESSION['player'];

			if($player->avatar != 'empty')  
				echo $player->avatar;
			else  
				echo '<img class = "avatar" src = "../img/avatars/noname.png">';	
		?>
		
		<div class = "row userData">
		
		<div class = "offset-lg-2 col-lg-4 equipment">
			<h2>
				<br/>Equipment:<br/>
			</h2>
			
			<div class = "row">
				<div class = "col-md-12">
				<?php

					$equipment = new Item();
					$equipment = $_SESSION['players_items'];

					$helmet = $equipment[1];
					$helmet -> display(1);
				?>	
				</div>
			</div>
			
			<div class = "row">
				<div class = "col-md-12">
				<?php

					$equipment = new Item();
					$equipment = $_SESSION['players_items'];

					$weapon = $equipment[2];
					$weapon -> display(2);

					$armor = $equipment[3];
					$armor -> display(2);

					$shield = $equipment[4];
					$shield -> display(2);
				?>	
				</div>
			</div>
			
			<div class = "row">
				<div class = "col-md-12">
				<?php

					$equipment = new Item();
					$equipment = $_SESSION['players_items'];

					$boots = $equipment[5];
					$boots -> display(1);
				?>	
				</div>
			</div>
			
		</div>
		
		<div class = "col-lg-4 attributes">
		
			<h2>
				<br/>Attributes:<br/>
			</h2>
			
			<h3 class = "playerStats">
				<form action = "../php/stat_plus.php" method = "post">
					<?php    
						$player = new Player();
						$player = $_SESSION['player'];

						echo ' Health: '.$player->health."(".$player->full_health.")";
					?>
				</form>
				
				<form action = "../php/stat_plus.php" method = "post">
					<?php    
						echo ' Attack: '.$player->attack."(".$player->full_attack.")";
						
						if($player->spare_points > 0)
						{
							echo '<div class = "hiddenInput"><input type = "text" name = "stat" value="2"></div>';
							echo '<input type = "submit" value = "+" class = "statBtn"/>';
						}
					?>
				</form>
				
				<form action = "../php/stat_plus.php" method = "post">
					<?php    
						echo ' Damage: '.$player->damage."(".$player->full_damage.")";
						
						if($player->spare_points > 0)
						{
							echo '<div class = "hiddenInput"><input type = "text" name = "stat" value="3"> <br/></div>';
							echo '<input type = "submit" value = "+" class = "statBtn"/>';
						}
					?>
				</form>
				
				<form action = "../php/stat_plus.php" method = "post">
					<?php    
						echo 'Agility: '.$player->agility."(".$player->full_agility.")";
						
						if($player->spare_points > 0)
						{
							echo '<div class = "hiddenInput"><input type = "text" name = "stat" value="4"> <br/></div>';
							echo '<input type = "submit" value = "+" class = "statBtn"/>';
						}
					?>
				</form>
				
				<form action = "../php/stat_plus.php" method = "post">
					<?php    
						echo 'Armor: '.$player->armor."(".$player->full_armor.")";
						
						if($player->spare_points > 0)
						{
							echo '<div class = "hiddenInput"><input type = "text" name = "stat" value="5"> <br/></div>';
							echo '<input type = "submit" value = "+" class = "statBtn"/>';
						}
					?>
				</form>
				
				<br>
				<?php	echo "Spare Points: ".$player->spare_points; 	?>
				
			</h3>
			
			</div>
		
		</div>

		<article id = "navMenu">
			<br><br><a href = "skills.php"><div class = "baseBtn">SKILLS</div></a>
			<br><a href = "home.php"><div class = "baseBtn">BACK</div></a><br/>
		</article>
	</main>
	
	<script src="../static/js/jqmin.js"></script>
    <script src="../static/js/bootstrap.min.js"></script>

</body>
</html>