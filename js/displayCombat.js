function displayCombat(delay)
{
 
    $.ajax(
    {
        url: "../php/combat_broker.php",
        cache: false,

        success: function (json)
        {
            //alert(json);
            try
            {
                //alert(json.replace(/\s/g, ' '));
                const battleLog = JSON.parse(json.replace(/\s/g, ' '));

                for (let i = 1; i < battleLog.length; i++)
                {
                    let k = i;
                    setTimeout(function ()
                    {
                        document.getElementById("cWin").innerHTML += battleLog[i];

                        var elem = document.getElementById('cWin');
                        elem.scrollTop = elem.scrollHeight;

                    }, delay * (k + 0));

                }
            }
            catch (err) {
                console.log("Error", err.stack);
                console.log("Error", err.name);
                console.log("Error", err.message);
                document.getElementById("cWin").innerHTML += 'busy - perss f5 or close all combat windows';
            }
        }
    });

    return false;
}
