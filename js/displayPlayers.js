function findPlayers()
{

    $.ajax({
        type: "POST",
        url: "../php/find_players.php",
        data: $('#searchingData').serialize() + '&signup_submit=2',
        cache: false,

        success: function (html) {

            $('#foundPlayers').html(html);
        }

    });


    return false;
}