﻿<?php

require_once('../php/SkillTree.php');

class Rival
{
	//general
	public $id;
	public $nick;
	public $avatar;

	//monster specific
	public $location;
	public $drop;

	//attributes
	public $level;
	public $health;
	public $attack;
	public $damage;
	public $agility;
	public $armor;
	public $full_health;
	public $full_attack;
	public $full_damage;
	public $full_agility;
	public $full_armor;
	public $skill_tree;

	//equipment
	public $eq_top;
	public $eq_left;
	public $eq_mid;
	public $eq_right;
	public $eq_bot;


	function initializeRival($id, $nick, $avatar, 
	$level, $health, $attack, $damage, $agility, $armor, $skill_tree,
	$e_top, $e_left, $e_mid, $e_right, $e_bot)
	{
		$this->id = $id;
		$this->nick = $nick;
		$this->avatar = $avatar;

		$this->level = $level;
		$this->health = $health;
		$this->attack = $attack;
		$this->damage = $damage;
		$this->agility = $agility;
		$this->armor = $armor;

		$this->eq_top = $e_top;
		$this->eq_left = $e_left;
		$this->eq_mid = $e_mid;
		$this->eq_right = $e_right;
		$this->eq_bot = $e_bot;

		$this->skill_tree = $skill_tree;
	}


	function generateMonster($connection, $location, $hardness)
	{	
		
		if($hardness == 0)								$temp_id = 0;
		else if(1   <= $hardness && $hardness <= 100)	$temp_id = 10*$_POST['location_id'] +1; 
		else if(101 <= $hardness && $hardness <= 200) 	$temp_id = 10*$_POST['location_id'] +2; 
		else if(201 <= $hardness && $hardness <= 300) 	$temp_id = 10*$_POST['location_id'] +3; 
		else if(301 <= $hardness && $hardness <= 415) 	$temp_id = 10*$_POST['location_id'] +4; 
		else if(416 <= $hardness && $hardness <= 480) 	$temp_id = 10*$_POST['location_id'] +5; 
		else if(481 <= $hardness && $hardness <= 600) 	$temp_id = 10*$_POST['location_id'] +6; 
		else if(601 <= $hardness && $hardness <= 700) 	$temp_id = 10*$_POST['location_id'] +7; 
		else if(701 <= $hardness && $hardness <= 800)	$temp_id = 10*$_POST['location_id'] +8; 
		else if(801 <= $hardness && $hardness <= 900)	$temp_id = 10*$_POST['location_id'] +9; 

		$sql_querry = 'SELECT * FROM enemies WHERE id='.$temp_id;
		$sql_answer = mysqli_query($connection, $sql_querry);
		$enemy_data = $sql_answer->fetch_assoc();


		$this->id = $enemy_data['id'];
		$this->level = $enemy_data['id'];
		$this->nick = $enemy_data['name'];
		$this->location = $enemy_data['loc']; 
		$this->drop = $enemy_data['drop']; 

		$this->full_health = $enemy_data['hp']   + rand(-$this->location, $this->location);
		$this->full_attack = $enemy_data['atc']  + rand(-$this->location, $this->location);
		$this->full_damage = $enemy_data['dmg']  + rand(-$this->location, $this->location);
		$this->full_agility = $enemy_data['agi'] + rand(-$this->location, $this->location);
		$this->full_armor = $enemy_data['arm']   + rand(-$this->location, $this->location);

	}


	function calculateAttributes($items)
	{
		$skill_tree = new SkillTree();
		$skill_tree = $_SESSION['skill_tree'];

		$this->full_health = $this->health + 
							$items[1]->health + 
							$items[2]->health + 
							$items[3]->health +
							$items[4]->health +
							$items[5]->health;

		$this->full_health = floor($this->full_health * (1 + $this->skill_tree[16]*$skill_tree->skills[16][2]));

		$this->full_damage = $this->damage +
							floor($this->damage * $this->skill_tree[1]*$skill_tree->skills[1][2]) +
							$items[1]->damage + 
							$items[2]->damage + 
							$items[3]->damage +
							$items[4]->damage +
							$items[5]->damage;

		$this->full_attack = $this->attack + 
							floor($this->attack * $this->skill_tree[1]*$skill_tree->skills[1][2]) +
							$items[1]->attack + 
							$items[2]->attack + 
							$items[3]->attack +
							$items[4]->attack +
							$items[5]->attack;

		$this->full_agility = $this->agility + 
							$items[1]->agility + 
							$items[2]->agility + 
							$items[3]->agility +
							$items[4]->agility +
							$items[5]->agility;

		$this->full_armor = $this->armor + 
							$items[1]->armor + 
							$items[2]->armor + 
							$items[3]->armor +
							$items[4]->armor +
							$items[5]->armor;	
	}
		
}

?>



