<?php

include 'Initializer.php';

session_start();
	
if(!isset($_SESSION['logged_in']))
{
	header('Location: index.php');
	exit();
}

//get player data
$player = new Player();
$player = $_SESSION['player'];

	
//connecting to database
require_once "db_data.php";
$connection = @new mysqli($host, $db_user, $db_password, $db_name);
	
// getting item data
$item_id = $_POST['item_id'];
$answer_sql = @$connection->query("SELECT * FROM items WHERE id =".$item_id);
	
$item_data = mysqli_fetch_assoc($answer_sql);
	
//set shop message
$_SESSION['purchase_completed'] = true;

if($player->level < $item_data['lvl'])
{
	$answer_sql->free();
	$connection->close();
		
	$_SESSION['shop_message'] = '<span style="color:red;">You don`t meet level requirement!';
		
	if($item_data['rarity'] < 3)
		header('Location: ../pages/shop.php');
	else
		header('Location: ../pages/crafting.php');

	exit();
}
else if($player->gold < $item_data['cost'] ||
( $item_data['rarity'] == 3 && $player->unique_shards < $item_data['cost']) || 
( $item_data['rarity'] == 4 && ($player->gold < 20*$item_data['cost'] || $player->legendary_shards < $item_data['cost'])))
{
	$answer_sql->free();
	$connection->close();
		
	$_SESSION['shop_message'] = '<span style="color:red;">You better take a loan first!';
	
	if($item_data['rarity'] < 3)
		header('Location: ../pages/shop.php');
	else
		header('Location: ../pages/crafting.php');

	exit();
}
else
{	
	switch($item_data['type'])
	{
		case 1:
			$my_query = "UPDATE players SET I_TOP = ".$item_data['id']." WHERE PID = ".$_SESSION['id'];		
			mysqli_query($connection, $my_query);	
			break;
		case 2:
			$my_query = "UPDATE players SET I_LEFT = ".$item_data['id']." WHERE PID = ".$_SESSION['id'];		
			mysqli_query($connection, $my_query);
			break;
		case 3:
			$my_query = "UPDATE players SET I_MID = ".$item_data['id']." WHERE PID = ".$_SESSION['id'];		
			mysqli_query($connection, $my_query);
			break;
		case 4:
			$my_query = "UPDATE players SET I_RIGHT = ".$item_data['id']." WHERE PID = ".$_SESSION['id'];		
			mysqli_query($connection, $my_query);	
			break;
		case 5:
			$my_query = "UPDATE players SET I_BOT = ".$item_data['id']." WHERE PID = ".$_SESSION['id'];		
			mysqli_query($connection, $my_query);
			break;
	}

	//charge
	if($item_data['rarity'] == 3)
	{
		$new_uni_frag = $player->unique_shards - $item_data['cost'];
		$my_query = "UPDATE players SET UQ = ".$new_uni_frag." WHERE PID =".$_SESSION['id'];
		mysqli_query($connection, $my_query);

		$temp_message = 'You successfully crafted: '.$item_data['name'];
	}
	else if($item_data['rarity'] == 4)
	{
		$new_gold = $player->gold - 20*$item_data['cost'];
		$my_query = "UPDATE players SET GOLD = ".$new_gold." WHERE PID =".$_SESSION['id'];
		mysqli_query($connection, $my_query);
						
		$new_le_frag = $player->legendary_shards - $item_data['cost'];
		$my_query = "UPDATE players SET LE = ".$new_le_frag." WHERE PID =".$_SESSION['id'];
		mysqli_query($connection, $my_query);

		$temp_message = 'You successfully crafted: '.$item_data['name'];
	}
	else
	{
		$new_gold = $player->gold - $item_data['cost'];
		$my_query = "UPDATE players SET GOLD = ".$new_gold." WHERE PID =".$_SESSION['id'];
		mysqli_query($connection, $my_query);

		$temp_message = 'You successfully purchased: '.$item_data['name'];
	}

	$_SESSION['shop_message'] = $temp_message;
}
	
	
//(re)loading stats
$initializer = new Initializer;

$initializer -> initializePlayer($connection, 'Location: ../pages/shop.php', $_SESSION['id']);


$connection->close();
	
?>