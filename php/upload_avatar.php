<?php

include '../php/Initializer.php';

session_start();
	
if(!isset($_SESSION['logged_in']))
{
	header('Location: ../index.php');
	exit();
}

$target_file = basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) 
{
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) 
	{
        $uploadOk = 1;
    } else 
	{
        $uploadOk = 0;
		$_SESSION['avatar_message'] = "Your file is not an image";
    }
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 64000) 
{
    $_SESSION['avatar_message'] = "Your file is too large.";
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "png" && $imageFileType != "jpg" && $imageFileType != "gif")
{
    $_SESSION['avatar_message'] = "Your file is in wrong format.";
    $uploadOk = 0;
}

// if everything is ok, try to upload file
if ($uploadOk == 1)
{
	//update user record
	require_once "../php/db_data.php";
	$connection = new mysqli($host, $db_user, $db_password, $db_name);

	$new_avatar = addslashes(file_get_contents($_FILES["fileToUpload"]["tmp_name"]));

	$my_query1 = "UPDATE players SET AV = 1 WHERE PID = ".$_SESSION['id'];		
	$my_query2 = "UPDATE players SET AVIMG = '".$new_avatar."' WHERE PID = ".$_SESSION['id'];

    if (mysqli_query($connection, $my_query1) && mysqli_query($connection, $my_query2)) 
	{
		$_SESSION['avatar_message'] = "Avatar has been updated successfully.";

		/*$my_query3 = "SELECT * FROM players WHERE PID = ".$_SESSION['id'];
		$result = mysqli_query($connection, $my_query3);

		$avi = mysqli_fetch_array($result);
		$_SESSION['user_avatar_image'] = '<img src="data:image/jpeg;base64,'.base64_encode($avi['AVIMG']).'"/>';

		$_SESSION['avatar_message'] = $_SESSION['user_avatar_image'];*/

    } 
	else 
	{
        $_SESSION['avatar_message'] =  "Sorry, there was an error uploading your file.";
    }

	// (re)loading stats
	$init_object = new Initializer;
	$init_object -> initializePlayer($connection, '', $_SESSION['id']);

	$connection->close();
}

header('Location: ../pages/settings.php');

?>