<?php

	session_start();
	
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../index.php');
		exit();
	}

	$message = htmlentities($_POST['message'], ENT_QUOTES, "UTF-8");

	$postOK = true;

	//set up connection
	require_once "../php/db_data.php";
    $my_connection = @new mysqli($host, $db_user, $db_password, $db_name);


    //check for double post
    $my_query = 'SELECT PID FROM posts ORDER BY id DESC LIMIT 1';
	$last_post_id = mysqli_fetch_assoc(mysqli_query($my_connection, $my_query));
	
	if($_SESSION['id'] == $last_post_id['PID'])
	{
		$_SESSION['post_error'] = true;

		$_SESSION['forum_message'] = "You can't add two posts in a row!";

		header('Location: ../pages/forum.php');
		exit();
	}


	//check if message is ok
	if(strlen($message) < 5)
	{
		$_SESSION['post_error'] = true;

		$_SESSION['forum_message'] = "Your message is too short!";

		header('Location: ../pages/forum.php');
		exit();
	}
	if(strlen($message) > 1023)
	{
		$_SESSION['post_error'] = true;

		$_SESSION['forum_message'] = "Your message is too long!";

		header('Location: ../pages/forum.php');
		exit();
	}

	//everthing ok - adding post
	$my_query = 'INSERT INTO posts (PID, CONTENT) VALUES ('.$_SESSION['id'].', "'.$message.'")';
	mysqli_query($my_connection, $my_query);

	header('Location: ../pages/forum.php');
	exit();
    
?>