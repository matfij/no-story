	<?php

	require_once "../php/Player.php";
	require_once "../php/db_data.php";
	session_start();

	//get post data
	$low_lvl = $_POST['lowLvl'];
	$high_lvl = $_POST['highLvl'];
	$rarity = $_POST['rarity'];
	$type = $_POST['type'];

	if(empty($low_lvl))
		$low_lvl = 1;
	if(empty($high_lvl))
		$high_lvl = 999;

	//connecting to database
	$connection = @new mysqli($host, $db_user, $db_password, $db_name);

	//get deisred items
	if($rarity == 0)
		$my_answer = $connection->query("SELECT * FROM items WHERE type =".$type." 
			AND lvl BETWEEN ".$low_lvl." AND ".$high_lvl." ORDER BY lvl");
	else
		$my_answer = $connection->query("SELECT * FROM items WHERE type =".$type." AND rarity =".$rarity."
			AND lvl BETWEEN ".$low_lvl." AND ".$high_lvl." ORDER BY lvl");

	error_reporting(0);
	$no_items = mysqli_num_rows($my_answer);

	if($no_items > 0)
	{
		$player = new Player();
		$player = $_SESSION['player'];
				
		echo '<div class = "col-lg-12 col-md-12 foundItems"><h2>Searching results:</h2>';

		for($cnt = 1; $cnt <= $no_items; $cnt++)
		{
			$item_data = mysqli_fetch_assoc($my_answer);

			$temp_item = new Item();
	
				$temp_item -> initialize(
				$item_data['id'],
				$item_data['name'],
				$item_data['rarity'],
				$item_data['type'],
				$item_data['gif'],
				$item_data['lvl'],
				$item_data['hp'],
				$item_data['atc'],
				$item_data['dmg'],
				$item_data['agi'],
				$item_data['arm'],
				$item_data['cost']
				);

					
			$temp_item -> generateShopLabel($player->level, $player->gold, $player->unique_shards, $player->legendary_shards);

			if($type == 1 || $type == 5)
			{
				echo '<div class = "eqSmall itemShop"> 
					<div class = "itemForm">
						<div class = "hiddenInput"><input type = "text" name = "item_id" value='.$temp_item->id.'></div>
						<img class = "itemEqS" src = "../img/items/'.$temp_item->image.'.gif"/>
						<div class = "itemLabel">'.$temp_item->shop_label.'</div>
					</div></div>';
			}
			else
			{
				echo '<div class = "eqLarge itemShop"> 
					<div class = "itemForm">
						<div class = "hiddenInput"><input type = "text" name = "item_id" value='.$temp_item->id.'></div>
						<img class = "itemEqL" src = "../img/items/'.$temp_item->image.'.gif"/>
						<div class = "itemLabel">'.$temp_item->shop_label.'</div>
					</div></div>';
			}

		}		
		echo '</div>';
	}
	else
	{
		echo '<div class = "warningMessage"><h4>No items founds</h4></div>';
	}

	

	?>