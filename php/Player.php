﻿<?php

require_once('../php/Item.php');
require_once('../php/SkillTree.php');

class Player
{
	//general
	public $id;
	public $nick;
	public $avatar;

	//possesion
	public $stamina;
	public $gold;
	public $unique_shards;
	public $legendary_shards;
	public $spare_points;
	public $spare_skill_points;

	//attributes
	public $level;
	public $experience;
	public $health;
	public $attack;
	public $damage;
	public $agility;
	public $armor;
	public $full_health;
	public $full_attack;
	public $full_damage;
	public $full_agility;
	public $full_armor;
	public $skill_tree;

	//equipment
	public $eq_top;
	public $eq_left;
	public $eq_mid;
	public $eq_right;
	public $eq_bot;


	function initialize($id, $nick, $avatar, 
	$stam, $gold, $unique_shards, $legendary_shards, $spare_points, $spare_skill_points,
	$level, $experience, $health, $attack, $damage, $agility, $armor,
	$e_top, $e_left, $e_mid, $e_right, $e_bot, $skill_tree)
	{
		$this->id = $id;
		$this->nick = $nick;
		$this->avatar = $avatar;

		$this->stamina = $stam;
		$this->gold = $gold;
		$this->unique_shards = $unique_shards;
		$this->legendary_shards = $legendary_shards;
		$this->spare_points = $spare_points;
		$this->spare_skill_points = $spare_skill_points;

		$this->level = $level;
		$this->experience = $experience;
		$this->health = $health;
		$this->attack = $attack;
		$this->damage = $damage;
		$this->agility = $agility;
		$this->armor = $armor;

		$this->eq_top = $e_top;
		$this->eq_left = $e_left;
		$this->eq_mid = $e_mid;
		$this->eq_right = $e_right;
		$this->eq_bot = $e_bot;

		$this->skill_tree = $skill_tree;
	}


	function calculateAttributes($items)
	{
		$skill_tree = new SkillTree();
		$skill_tree = $_SESSION['skill_tree'];

		$this->full_health = $this->health + 
							$items[1]->health + 
							$items[2]->health + 
							$items[3]->health +
							$items[4]->health +
							$items[5]->health;

		$this->full_health = floor($this->full_health * (1 + $this->skill_tree[16]*$skill_tree->skills[16][2]));

		$this->full_damage = $this->damage +
							floor($this->damage * $this->skill_tree[1]*$skill_tree->skills[1][2]) +
							$items[1]->damage + 
							$items[2]->damage + 
							$items[3]->damage +
							$items[4]->damage +
							$items[5]->damage;

		$this->full_attack = $this->attack + 
							floor($this->attack * $this->skill_tree[1]*$skill_tree->skills[1][2]) +
							$items[1]->attack + 
							$items[2]->attack + 
							$items[3]->attack +
							$items[4]->attack +
							$items[5]->attack;

		$this->full_agility = $this->agility + 
							$items[1]->agility + 
							$items[2]->agility + 
							$items[3]->agility +
							$items[4]->agility +
							$items[5]->agility;

		$this->full_armor = $this->armor + 
							$items[1]->armor + 
							$items[2]->armor + 
							$items[3]->armor +
							$items[4]->armor +
							$items[5]->armor;	
	}


		
}

?>



