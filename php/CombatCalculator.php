<?php

require_once('../php/SkillTree.php');

class CombatCalculator
{

	public $combat_result;


	function executeCombat($player, $rival, $reporting)
	{
		$skill_tree = new SkillTree();
		$skill_tree = $_SESSION['skill_tree'];

		//combat parameters
		$tour_limit = 30;
		$fight_on = true;
		$tour_counter = 1;
		$battle_result[0] = 'start';

		//player data
		$player_health = $player->full_health;
		$player_attack = $player->full_attack;
		$player_damage = $player->full_damage;
		$player_agility = $player->full_agility;
		$player_armor = $player->full_armor;

		$player_time = $player_agility;
		$player_crit = floor((10 * $player_attack) / (1.5 * $player->level)) + $player->skill_tree[2] * $skill_tree->skills[2][2];
		$player_piercing = $player->skill_tree[3] * $skill_tree->skills[3][2];
		$player_inevitability = $player->skill_tree[4] * $skill_tree->skills[4][2];
		$player_poison = round($player->skill_tree[5] * $skill_tree->skills[5][2] * ($player->level / 12), 2);
		$player_link = $player->skill_tree[6] * $skill_tree->skills[6][2];
		$player_smashing = $player->skill_tree[7] * $skill_tree->skills[7][2];
		$player_braking = $player->skill_tree[8] * $skill_tree->skills[8][2] * rand($player->level/16 + 1, $player->level/14 + 1);
		$player_stun = $player->skill_tree[9] * $skill_tree->skills[9][2];
		$player_misfortune = $player->skill_tree[17] * $skill_tree->skills[17][2];
		$player_evade = $player->skill_tree[18] * $skill_tree->skills[18][2];
		$player_anchor = $player->skill_tree[19] * $skill_tree->skills[19][2];
		$player_healing = $player->skill_tree[20] * $skill_tree->skills[20][2] * $player_health;
		$player_protection = $player->skill_tree[21] * $skill_tree->skills[21][2];
		$player_block = $player->skill_tree[22] * $skill_tree->skills[22][2];

		//rival data
		$rival_health = $rival->full_health;
		$rival_attack = $rival->full_attack;
		$rival_damage = $rival->full_damage;
		$rival_agility = $rival->full_agility;
		$rival_armor = $rival->full_armor;

		$rival_agility = (1-$player_anchor) * $rival_agility;

		$rival_time = $rival_agility;

		//location boost
		if($rival->location > floor($player->level/10) + 1)
		{
			$rival_attack = 2 * $rival_attack;
			$rival_agility = 1.5 * $rival_agility;
		}



		while($fight_on)
		{			
			if($player_time >= $rival_time)  // PLAYERS TURN
			{
				if($player->stamina > 0)
				{
					//anchor effect
					$player_time = $player_time - $rival_agility;
									
					if($player_attack * rand(40 + $player_inevitability, 80 + $player_inevitability) > $rival_agility * rand(25, 65)) 
					{
						//check for critical strike
						$chance = rand(0, 90);
						if($player_crit > $chance)
							$critical = rand(16 + $player_smashing, 20 + $player_smashing)/10;	
						else
							$critical = 1;

						//check fo piercing strike
						$chance = rand(0, 90);
						$pierced = false;
						if($player_piercing > $chance)
						{
							$pierced = true;
							$true_dmg = floor($critical * $player_damage - 0);
						}	
						else
							$true_dmg = floor($critical * $player_damage - ($rival_armor * rand(75,95))/100);

						if($true_dmg < 1)  
							$true_dmg = rand(1, 2 + floor($player->level/10));
										
						$rival_health = $rival_health - $true_dmg;

						//life link
						if($player_link != 0)
						{
							$player_health += $true_dmg*$player_link;

							if($player_health > $player->full_health)
								$player_health = $player->full_health;
						}
						
						if($critical != 1)
						{
							$battle_result[$tour_counter] = '<b><div style="color: #007fff;">'.$player->nick.'['.round($player_health, 2).
							'] CRITES '.$rival->nick.'['.round($rival_health, 2).'] for '.$true_dmg.' damage!<br/></div></b>';
						}
						else
						{
							$battle_result[$tour_counter] = '<div style="color: #9999ff;">'.$player->nick.'['.round($player_health, 2).']'.
							' hits '.$rival->nick.'['.round($rival_health, 2).'] for '.$true_dmg.' damage.<br/></div>';
						}

						//armor breaker
						if($player_braking != 0 && $rival_armor > 0)
						{
							$rival_armor = $rival_armor - $player_braking;

							$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #9999ff; font-size:16px;">'.
							'<b> + '.$player_braking.' armor break</b></div>';

							if($rival_armor < 0)
								$rival_armor = 0;
						}
						
						// life link
						if($player_link != 0)
							$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #9999ff; font-size:16px;">'.
							'<b> + '.round($true_dmg*$player_link, 2).' life link</b></div>';

						//stun
						$chance = rand(0, 90);
						if($player_stun > $chance)
						{
							$rival_time = $rival_time - $rival_agility;

							$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #9999ff; font-size:16px;">'.
							'<b> + stun</b></div>';
						}

						if($pierced)
							$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #9999ff; font-size:16px;">
							<b>+ piercing strike</b><br/></div>';
				
										
						if($rival_health < 1)  
						{
							$fight_on = false;
							$this->combat_result = 1;
						}
					}
					else	$battle_result[$tour_counter] = '<div style="color: #9999ff;">'.$player->nick.' misses.<br/></div>';
				}
				else  
				{
					$player_time = $player_time - $rival_agility;
					$battle_result[$tour_counter] = '<div style="color: #9999ff;">'.$player->nick.' is too tired to move.<br/></div>';
				}
				$battle_result[$tour_counter] = $battle_result[$tour_counter].'---';
			}
							
			else  // ENEMY TURN
			{
				$rival_time = $rival_time - $player_agility;
								
				if($rival_attack*rand(40-$player_evade, 80-$player_evade) > $player_agility*rand(25, 65)) 
				{
					//check for critical strike
					$chance = floor((10*$rival_attack) / $rival->level);
					$p = rand(0+$player_misfortune, 90+$player_misfortune);

					if($chance > $p)
					{
						$rival_critical = rand(18-$player_protection, 22-$player_protection)/10;
					}	
					else
						$rival_critical = 1;

					$true_dmg = floor($rival_critical * $rival_damage - ($player_armor * rand(75,95))/100);

					if($true_dmg < 1)  
						$true_dmg = rand(1, 2 + floor($player->level/10));
						
					//player block
					$chance = rand(0, 90);
					$blocked = false;
					if($player_block > $chance)
					{
						$true_dmg = 0.15*$true_dmg;
						$blocked = true;
					}

					$player_health = $player_health - $true_dmg;

					if($rival_critical > 1)
					{
						$battle_result[$tour_counter] = '<b><div style="color: #EE7600;">'.$rival->nick.'['.round($rival_health, 2).']'.' CRITES '.$player->nick.'['.round($player_health, 2).'] 
							for '.$true_dmg.' damage!<br/></div></b>';
					}
					else
					{
						$battle_result[$tour_counter] = '<div style="color: #ffd27f;">'.$rival->nick.'['.round($rival_health, 2).']'.' hits '.$player->nick.'['.round($player_health, 2).'] 
							for '.$true_dmg.' damage!<br/></div>';
					}
					if($blocked)
						$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #ffd27f; font-size:16px;">
						<b>- block</b><br/></div>';
					
																			
					if($player_health < 1)  
					{
						$this->combat_result = 2;
						$fight_on = false;
					}

				}
				else	$battle_result[$tour_counter] = '<div style="color: #ffd27f;">'.$rival->nick.' misses.<br/></div>';

				//poison effect
				if($player_poison != 0 && $fight_on && $player->stamina > 0)
				{
					$rival_health = $rival_health - $player_poison;

					$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #66CDAA; font-size:16px;">'
					.$rival->nick.'['.round($rival_health, 2).'] suffered '.$player_poison.' damage due to the poison.<br/></div>';

					if($rival_health < 1)  
					{
						$fight_on = false;
						$this->combat_result = 1;
					}
				}
				$battle_result[$tour_counter] = $battle_result[$tour_counter].'---';
			}

			//player healing
			if($player_healing != 0)
			{
				$player_health += $player_healing;

				if($player_health > $player->full_health)
					$player_health = $player->full_health;
			}
							
			$tour_limit--;
			$tour_counter++;

			if($tour_limit < 0 && $fight_on) 
			{
				$this->combat_result = 0;
				$fight_on = false;
			}
		}


		if($reporting == 1)
		{
			for($cnt = 1; $cnt < sizeof($battle_result); $cnt++)
			{
				echo $battle_result[$cnt];
			}
		}
		else
		{
			switch($this->combat_result)
			{
				case 0: $battle_result[$tour_counter] = '<br/><h3><b>Drow</b></h3>';
				break;

				case 1: $battle_result[$tour_counter] = '<br/><h3><b>You won</b></h3>';
				break;

				case 2: $battle_result[$tour_counter] = '<br/><h3><b>You lost</b></h3>';
				break;
			} 

			$_SESSION['battle_log'] = json_encode($battle_result);
		}
			
		
	}


	function executeArenaCombat($player, $rival, $reporting)
	{
		$skill_tree = new SkillTree();
		$skill_tree = $_SESSION['skill_tree'];

		//combat parameters
		$tour_limit = 30;
		$fight_on = true;
		$tour_counter = 1;
		$battle_result[0] = 'start';

		//player data
		$player_health = $player->full_health;
		$player_attack = $player->full_attack;
		$player_damage = $player->full_damage;
		$player_agility = $player->full_agility;
		$player_armor = $player->full_armor;

		$player_crit = floor((10 * $player_attack) / $player->level) + $player->skill_tree[2] * $skill_tree->skills[2][2];
		$player_piercing = $player->skill_tree[3] * $skill_tree->skills[3][2];
		$player_inevitability = $player->skill_tree[4] * $skill_tree->skills[4][2];
		$player_poison = $player->skill_tree[5] * $skill_tree->skills[5][2] * rand($player->level/11, $player->level/9);
		$player_link = $player->skill_tree[6] * $skill_tree->skills[6][2];
		$player_smashing = $player->skill_tree[7] * $skill_tree->skills[7][2];
		$player_braking = $player->skill_tree[8] * $skill_tree->skills[8][2] * rand($player->level/16 + 1, $player->level/14 + 1);
		$player_stun = $player->skill_tree[9] * $skill_tree->skills[9][2];
		$player_misfortune = $player->skill_tree[17] * $skill_tree->skills[17][2];
		$player_evade = $player->skill_tree[18] * $skill_tree->skills[18][2];
		$player_anchor = $player->skill_tree[19] * $skill_tree->skills[19][2];
		$player_healing = $player->skill_tree[20] * $skill_tree->skills[20][2] * $player_health;
		$player_protection = $player->skill_tree[21] * $skill_tree->skills[21][2];
		$player_block = $player->skill_tree[22] * $skill_tree->skills[22][2];

		//rival data
		$rival_health = $rival->full_health;
		$rival_attack = $rival->full_attack;
		$rival_damage = $rival->full_damage;
		$rival_agility = $rival->full_agility;
		$rival_armor = $rival->full_armor;

		$rival_crit = floor((10 * $rival_attack) / $rival->level) + $rival->skill_tree[2] * $skill_tree->skills[2][2];
		$rival_piercing = $rival->skill_tree[3] * $skill_tree->skills[3][2];
		$rival_inevitability = $rival->skill_tree[4] * $skill_tree->skills[4][2];
		$rival_poison = $rival->skill_tree[5] * $skill_tree->skills[5][2] * rand($rival->level/11, $rival->level/9);
		$rival_link = $rival->skill_tree[6] * $skill_tree->skills[6][2];
		$rival_smashing = $rival->skill_tree[7] * $skill_tree->skills[7][2];
		$rival_braking = $rival->skill_tree[8] * $skill_tree->skills[8][2] * rand($rival->level/16 + 1, $rival->level/14 + 1);
		$rival_stun = $rival->skill_tree[9] * $skill_tree->skills[9][2];
		$rival_misfortune = $rival->skill_tree[17] * $skill_tree->skills[17][2];
		$rival_evade = $rival->skill_tree[18] * $skill_tree->skills[18][2];
		$rival_anchor = $rival->skill_tree[19] * $skill_tree->skills[19][2];
		$rival_healing = $rival->skill_tree[20] * $skill_tree->skills[20][2] * $rival_health;
		$rival_protection = $rival->skill_tree[21] * $skill_tree->skills[21][2];
		$rival_block = $rival->skill_tree[22] * $skill_tree->skills[22][2];

		//recalculating stats with skills
		$player_agility = (1-$rival_anchor) * $player_agility;
		$rival_agility = (1-$player_anchor) * $rival_agility;

		$player_time = $player_agility;
		$rival_time = $rival_agility;

		while($fight_on)
		{			
			if($player_time >= $rival_time)  // PLAYERS TURN
			{
				if($player->stamina > 0)
				{
					//anchor effect
					$player_time = $player_time - $rival_agility;
									
					if($player_attack * rand(40 + $player_inevitability-$rival_evade, 80 + $player_inevitability-$rival_evade) > $rival_agility * rand(25, 65)) 
					{
						//check for critical strike
						$chance = rand(0+$rival_misfortune, 90+$rival_misfortune);
						if($player_crit > $chance)
							$critical = rand(16+$player_smashing-$rival_protection, 20+$player_smashing-$rival_protection)/10;	
						else
							$critical = 1;

						//check true damage
						$true_dmg = floor($critical * $player_damage - ($rival_armor * rand(75,95))/100);

						if($true_dmg < 1)  
							$true_dmg = rand(1, 2 + floor($player->level/10));

						//rival block
						$chance = rand(0, 90);
						$blocked = false;
						if($rival_block > $chance)
						{
							$true_dmg = 0.15*$true_dmg;
							$blocked = true;
						}

						//check fo piercing strike
						$chance = rand(0, 90);
						$pierced = false;
						if($player_piercing > $chance)
						{
							$true_dmg = floor($critical * $player_damage);
							$pierced = true;
						}
										
						$rival_health = $rival_health - $true_dmg;

						//life link
						if($player_link != 0)
						{
							$player_health += $true_dmg * $player_link;

							if($player_health > $player->full_health)
								$player_health = $player->full_health;
						}
						
						if($critical != 1)
						{
							$battle_result[$tour_counter] = '<b><div style="color: #007fff;">'.$player->nick.'['.round($player_health, 2).
							'] CRITES '.$rival->nick.'['.round($rival_health, 2).'] for '.$true_dmg.' damage!<br/></div></b>';
						}
						else
						{
							$battle_result[$tour_counter] = '<div style="color: #9999ff;">'.$player->nick.'['.round($player_health, 2).']'.
							' hits '.$rival->nick.'['.round($rival_health, 2).'] for '.$true_dmg.' damage.<br/></div>';
						}

						//effect comunicator

						//armor breaker
						if($player_braking != 0 && $rival_armor > 0)
						{
							$rival_armor = $rival_armor - $player_braking;

							$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #9999ff; font-size:16px;">'.
							'<b> + '.$player_braking.' armor break</b></div>';

							if($rival_armor < 0)
								$rival_armor = 0;
						}	

						//stun
						$chance = rand(0, 90);
						if($player_stun > $chance)
						{
							$rival_time = $rival_time - $rival_agility;

							$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #9999ff; font-size:16px;">'.
							'<b> + stun</b></div>';
						}

						if($blocked)
							$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #9999ff; font-size:16px;">
							<b> - block</b><br/></div>';

						if($pierced)
							$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #9999ff; font-size:16px;">
							<b> + piercing strike</b><br/></div>';
								
										
						if($rival_health < 1)  
						{
							$fight_on = false;
							$this->combat_result = 1;
						}
					}
					else	$battle_result[$tour_counter] = '<div style="color: #9999ff;">'.$player->nick.' misses.<br/></div>';
				}
				else  
				{
					$player_time = $player_time - $rival_agility;
					$battle_result[$tour_counter] = '<div style="color: #9999ff;">'.$player->nick.' is too tired to move.<br/></div>';
				}

				//poison effect
				if($rival_poison != 0 && $fight_on)
				{
					$player_health = $player_health - $rival_poison;

					$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #66CDAA; font-size:16px;">'.$player->nick.
					'['.round($player_health, 2).'] suffered '.$rival_poison.' damage due to the poison.<br/></div>';

					if($player_health < 1)  
					{
						$fight_on = false;
						$this->combat_result = 2;
					}
				}
				$battle_result[$tour_counter] = $battle_result[$tour_counter].'---';
			}
							
			else  // ENEMY TURN
			{
				$rival_time = $rival_time - $player_agility;
								
				if($rival_attack*rand(40-$player_evade+$rival_inevitability, 80-$player_evade+$rival_inevitability) > $player_agility*rand(25, 65)) 
				{
					//check for critical strike
					$chance = rand(0+$player_misfortune, 90+$player_misfortune);
					if($rival_crit > $chance)
					{
						$rival_critical = rand(18-$player_protection+$rival_smashing, 22-$player_protection+$rival_smashing)/10;
					}	
					else
						$rival_critical = 1;

					//check true damage
					$true_dmg = floor($rival_critical * $rival_damage - ($player_armor * rand(75,95))/100);
					if($true_dmg < 1)  
						$true_dmg = rand(1, 2 + floor($player->level/10));
						
					//player block
					$chance = rand(0, 90);
					$blocked = false;
					if($player_block > $chance)
					{
						$true_dmg = 0.15*$true_dmg;
						$blocked = true;
					}

					//check fo piercing strike
					$chance = rand(0, 90);
					$pierced = false;
					if($rival_piercing > $chance)
					{
						$true_dmg = floor($rival_critical * $rival_damage);
						$pierced = true;
					}	

					$player_health = $player_health - $true_dmg;

					//life link
					if($rival_link != 0)
					{
						$rival_health += $true_dmg * $rival_link;

						if($rival_health > $rival->full_health)
							$rival_health = $rival->full_health;
					}

					if($rival_critical > 1)
					{
						$battle_result[$tour_counter] = '<b><div style="color: #EE7600;">'.$rival->nick.'['.round($rival_health, 2).
						']'.' CRITES '.$player->nick.'['.round($player_health, 2).'] for '.$true_dmg.' damage!<br/></div></b>';
					}
					else
					{
						$battle_result[$tour_counter] = '<div style="color: #ffd27f;">'.$rival->nick.'['.round($rival_health, 2).']'.
						' hits '.$player->nick.'['.round($player_health, 2).'] for '.$true_dmg.' damage!<br/></div>';
					}

					//effect comunicator

					if($rival_braking != 0 && $player_armor>0)
					{
						$player_armor = $player_armor - $rival_braking;

						$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #ffd27f; font-size:16px;">
						<b>+ '.$rival_braking.' armor break</b><br/></div>';

						if($player_armor < 0)
							$player_armor = 0;
					}

					//stun
					$chance = rand(0, 90);
					if($rival_stun > $chance)
					{
						$player_time = $player_time - $player_agility;

						$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #ffd27f; font-size:16px;">'.
						'<b> + stun</b></div>';
					}

					if($blocked)
						$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #ffd27f; font-size:16px;">
						<b>- block</b><br/></div>';

					if($pierced)
						$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #ffd27f; font-size:16px;">
						<b>+ piercing strike</b><br/></div>';

														
					if($player_health < 1)  
					{
						$this->combat_result = 2;
						$fight_on = false;
					}

				}
				else	$battle_result[$tour_counter] = '<div style="color: #ffd27f;">'.$rival->nick.' misses.<br/></div>';

				//poison effect
				if($player_poison != 0 && $fight_on && $player->stamina > 0)
				{
					$rival_health = $rival_health - $player_poison;

					$battle_result[$tour_counter] = $battle_result[$tour_counter].'<div style="color: #66CDAA; font-size:16px;">'.$rival->nick.
					'['.round($rival_health, 2).'] suffered '.$player_poison.' damage due to the poison.<br/></div>';

					if($rival_health < 1)  
					{
						$fight_on = false;
						$this->combat_result = 1;
					}
				}
				$battle_result[$tour_counter] = $battle_result[$tour_counter].'---';
			}

			//player healing
			if($player_healing != 0)
			{
				$player_health += $player_healing;

				if($player_health > $player->full_health)
					$player_health = $player->full_health;
			}
			//rival healing
			if($rival_healing != 0)
			{
				$rival_health += $rival_healing;

				if($rival_health > $rival->full_health)
					$rival_health = $rival->full_health;
			}
							
			$tour_limit--;
			$tour_counter++;

			if($tour_limit < 0 && $fight_on) 
			{
				$this->combat_result = 0;
				$fight_on = false;
			}
		}


		if($reporting == 1)
		{
			for($cnt = 1; $cnt < sizeof($battle_result); $cnt++)
			{
				echo $battle_result[$cnt];
			}
		}
		else
		{
			switch($this->combat_result)
			{
				case 0: $battle_result[$tour_counter] = '<br/><h3><b>Drow</b></h3>';
				break;

				case 1: $battle_result[$tour_counter] = '<br/><h3><b>You won</b></h3>';
				break;

				case 2: $battle_result[$tour_counter] = '<br/><h3><b>You lost</b></h3>';
				break;
			} 

			$_SESSION['battle_log'] = json_encode($battle_result);
		}
			
		
	}


	function saveResults($connection, $player, $rival)
	{
		$skill_tree = new SkillTree();
		$skill_tree = $_SESSION['skill_tree'];

		$player_protection = $player->skill_tree[10] * $skill_tree->skills[10][2];
		$player_treasure = $player->skill_tree[12] * $skill_tree->skills[12][2];
		$player_adept = $player->skill_tree[13] * $skill_tree->skills[13][2];
		$player_cunning = $player->skill_tree[15] * $skill_tree->skills[15][2];

		$level_advantage = floor(($player->level - 10*$rival->location) / 10) + 1;

		if($level_advantage == 0)
		{
			$dropdown = 1;
			$sharddown = 1;
		}
		else if($level_advantage == 1)
		{
			$dropdown = 0.7;
			$sharddown = 0.9;
		}
		else if($level_advantage == 2)
		{
			$dropdown = 0.5;
			$sharddown = 0.75;
		}
		else if($level_advantage == 3)
		{
			$dropdown = 0.25;
			$sharddown = 0.5;
		}
		else
		{
			$dropdown = 0.1;
			$sharddown = 0.25;
		}

		if($this->combat_result == 0)
		{
			$extra_exp = floor( $dropdown * (floor(1 + $rival->drop/3) + rand(-$rival->location, $rival->location)));
			if($extra_exp < 2)	$extra_exp = $rival->location;
			
			echo '<br/><h4>Drow!
			<br/>Exp + '.$extra_exp.'</h4>';
			$extra_gold = 0;
		}

		else if($this->combat_result == 1)
		{
			$extra_exp = (1+$player_adept)*$rival->drop + rand($rival->location, 3*$rival->location);
			$extra_exp = floor($dropdown * $extra_exp);
			if($extra_exp < 1) 
				$extra_exp = 1;

			$extra_gold = (1+$player_treasure)*$rival->drop + rand(-$rival->location, $rival->location);
			$extra_gold = floor($dropdown * $extra_gold);
			if($extra_gold < 1) 
				$extra_gold = 1;

			echo '<br/><h4>You won!
			<br/>Gold + '.$extra_gold.'
			<br/>Exp + '.$extra_exp.'</h4>';
							
			//SPECIAL DROP
			if($rival->id%10 == 5)
			{
				$extra_un_shards = floor( $sharddown * (floor(rand($rival->drop/3, $rival->drop/2)) + 5 + $rival->location));
				echo '<br/><span style="color:#0080ff;"><h4>You found '.$extra_un_shards.' unique shards!</span>';
			}
			if($rival->id%10 == 9)
			{
				$extra_le_shards = floor(rand($rival->drop/9, $rival->drop/6));
				echo '<h3><span style="color:#ffae19;">You found '.$extra_le_shards.' Legendary Shards!</h3><br/></span>';
			}
		}

		else if($this->combat_result == 2)
		{
			$extra_exp = 0;

			if($rival->location == 1) 
				$extra_gold = -floor(rand(2, 5));
			else	 
				$extra_gold = floor(($player_protection-1)*rand(9*$rival->location, 14*$rival->location));
							
			if(abs($extra_gold) > abs($player->gold))  
				$extra_gold = -$player->gold;
							
			echo '<br/><h4>You lost!<br/>
			Enemy has stolen '.abs($extra_gold).' gold.</h4>';
		}
		

		//UPDATING PLAYER DATA

		//cunning stamina 
		$chance = rand(0,90);
		if($player_cunning > $chance)
			$new_stamina = $player->stamina;
		else
			$new_stamina = $player->stamina - 1;

		if($new_stamina < 1)  $new_stamina = 0;
		$my_query = "UPDATE players SET STAM = ".$new_stamina." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
							
		$new_exp = $player->experience + $extra_exp;
		$my_query = "UPDATE players SET EXP = ".$new_exp." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);

		$new_gold = $player->gold + $extra_gold;
		if($new_gold < 1)  $new_gold = 0;
		$my_query = "UPDATE players SET GOLD = ".$new_gold." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
							
		$new_uni_frag = $player->unique_shards + $extra_un_shards;
		$my_query = "UPDATE players SET UQ = ".$new_uni_frag." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
							
		$new_le_frag = $player->legendary_shards + $extra_le_shards;
		$my_query = "UPDATE players SET LE = ".$new_le_frag." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
				

		//CHECK IF LEVEL UP
		$my_query = "SELECT * FROM levels";	
		$sql_answer = mysqli_query($connection, $my_query);
		$lvl_number = mysqli_num_rows($sql_answer);
							
		$extra_sp = 0;
		$extra_spp = 0;
							
		for ($i = 1; $i <= $lvl_number; $i++) 
		{
			$row = mysqli_fetch_assoc($sql_answer);
								
			if($new_exp >= $row['exp'] && $row['lvl'] > $player->level)
			{
				echo '<br><h4> <div style="color: #ffd27f;">Next level: '.$row['lvl'].' !<br/>Attributes +3
				<br/>Skill points +1<h4></div>';
				$new_lvl = $row['lvl'];
				$extra_sp = $extra_sp + 3;
				$extra_spp = $extra_spp + 1;
			}
		}
		//updating stats after level up
		$my_query = "UPDATE players SET LVL = ".$new_lvl." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
							
		$new_sp = $player->spare_points + $extra_sp;
		$my_query = "UPDATE players SET SP = ".$new_sp." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);

		$new_spp = $player->spare_skill_points + $extra_spp;
		$my_query = "UPDATE players SET SPP = ".$new_spp." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
							

		// (re)loading stats
		$initializer = new Initializer();
		$initializer->initializePlayer($connection, '../pages/base_location.php', $_SESSION['id']);
						
	}

}