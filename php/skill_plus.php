<?php

include 'Initializer.php';

session_start();
	
if(!isset($_SESSION['logged_in']))
{
	header('Location: ../index.php');
	exit();
}

	
//connecting to database
require_once "db_data.php";
$connection = @new mysqli($host, $db_user, $db_password, $db_name);
	
$player = new Player();
$player = $_SESSION['player'];

$skill_id = $_POST['skill_id'];

//UPDATE SKILL
for($cnt = 1; $cnt < $skill_id+2; $cnt++)
{
	if($cnt == $skill_id && $player->spare_skill_points > 0 && $player->skill_tree[$cnt] < 10)
	{
		$new_spp = $player->spare_skill_points - 1;
		$sql_query = "UPDATE players SET SPP = ".$new_spp." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $sql_query);

		$new_skill_level = $player->skill_tree[$cnt] + 1;
		$sql_query = "UPDATE skilltrees SET s".$cnt." = ".$new_skill_level." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $sql_query);
	}
}
		
//(re)initialize player
$init_object = new Initializer;
$init_object -> initializePlayer($connection, 'Location: ../pages/skills.php', $player->id);

$answer_sql->free();	
$connection->close();
	
?>