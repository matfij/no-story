﻿<?php

include '../php/Player.php';

class Initializer
{
	function initializePlayer($connection, $destination, $pid)
	{
		//EXECUTE QUERY
		$my_query = 'SELECT * FROM players WHERE PID = '.$pid;	
		$answer_sql = mysqli_query($connection, $my_query);

		$user_data = mysqli_fetch_array($answer_sql);


		//LOAD AVATAR
		if($user_data['AV'] == 1)
			$user_data['AVATAR'] = '<img class = "avatar" src="data:image/jpeg;base64,'.base64_encode($user_data['AVIMG']).'"/>';
		else
			$user_data['AVATAR'] = 'empty';


		//LOADING SKILL TREE
		$answer_sql = $connection->query("SELECT * FROM skilltrees WHERE PID = ".$_SESSION['id']);
		$player_skill_tree = mysqli_fetch_array($answer_sql);

		//LOADING SPECIFIC SKILLS
		if(!isset($_SESSION['skill_tree']))
		{
			$skill_tree = new SkillTree();
			$skill_tree->initializeSkillTree($connection);
			$_SESSION['skill_tree'] = $skill_tree;
		}
		else
		{
			$skill_tree = new SkillTree();
			$skill_tree = $_SESSION['skill_tree'];
		}

		//UPDATE STAMINA
		$max_stam = 200 + $player_skill_tree[11]*$skill_tree->skills[11][2];
		if($user_data['STAM'] < $max_stam)
		{
			$nextDay = date('d');
			$nextHour = date('H');
			$nextMin = date('i');

			$lastDay = floor($user_data['STAM_GAIN']/10000);
			$lastHour = floor(floor($user_data['STAM_GAIN']/100)%100);
			$lastMin = floor($user_data['STAM_GAIN']%100);

			$dAlter = $nextDay-$lastDay;
			$hAlter = $nextHour-$lastHour;
			$mAlter = $nextMin-$lastMin;

			$timeAlter = floor(0.5*(1440*$dAlter + 60*$hAlter +$mAlter));
			$nextTime = $nextDay.$nextHour.$nextMin;
			$stamGain =  $user_data['STAM']+$timeAlter;

			if($stamGain > $max_stam)
				$stamGain = $max_stam;

			if($stamGain < 0)
				$stamGain = 100;

			@$connection->query("UPDATE players SET STAM_GAIN = ".$nextTime." WHERE PID = ".$_SESSION['id']);
			@$connection->query("UPDATE players SET STAM = ".$stamGain." WHERE PID = ".$_SESSION['id']);

			$user_data['stam'] = $stamGain;
		}
		else
			$user_data['stam'] = $user_data['STAM'];


		//LOADING ITEMS
		$answer_sql = $connection->query("SELECT * FROM items WHERE 
		id =".$user_data['I_BOT']." OR id=".$user_data['I_TOP']." OR id=".$user_data['I_MID']." 
		OR id=".$user_data['I_LEFT']." OR id=".$user_data['I_RIGHT']."
		ORDER BY type");

		$empty_item = new Item();
		$first_empty = 1;

		for($cnt = 1; $cnt <= 5; $cnt++)
		{
			$item_data = mysqli_fetch_assoc($answer_sql);

			$temp_item = new Item();

			$temp_item -> initialize(
			$item_data['id'],
			$item_data['name'],
			$item_data['rarity'],
			$item_data['type'],
			$item_data['gif'],
			$item_data['lvl'],
			$item_data['hp'],
			$item_data['atc'],
			$item_data['dmg'],
			$item_data['agi'],
			$item_data['arm'],
			$item_data['cost']
			);

			$temp_item -> generateLabel();

			if($item_data['type'] == 1)  
				$temp_items[1] = $temp_item;

			else if($item_data['type'] == 2)  
				$temp_items[2] = $temp_item;
				
			else if($item_data['type'] == 3)  
				$temp_items[3] = $temp_item; 

			else if($item_data['type'] == 4)  
				$temp_items[4] = $temp_item;  

			else if($item_data['type'] == 5)  
				$temp_items[5] = $temp_item; 

			else
			{
				if($first_empty == 1)
				{
					$empty_item = $temp_item;
					$first_empty = 0;
				}	
				$temp_items[$cnt] = $empty_item;
			}	
		}

		$_SESSION['players_items'] = $temp_items;


		//SAVE PLAYER DATA
		$player = new Player();

		$player -> initialize(
		$user_data['PID'], 
		$user_data['NICK'],
		$user_data['AVATAR'],
		$user_data['stam'],
		$user_data['GOLD'],
		$user_data['UQ'],
		$user_data['LE'],
		$user_data['SP'],
		$user_data['SPP'],
		$user_data['LVL'],
		$user_data['EXP'],
		$user_data['HP'],
		$user_data['ATC'],
		$user_data['DMG'],
		$user_data['AGI'],
		$user_data['ARM'],
		$user_data['I_TOP'],
		$user_data['I_LEFT'],
		$user_data['I_MID'],
		$user_data['I_RIGHT'],
		$user_data['I_BOT'],
		$player_skill_tree
		);

		$player -> calculateAttributes($temp_items);

		$_SESSION['player'] = $player;
		
		$answer_sql->free();

		if ($destination != '') 
		{
			header($destination);
		}
	}



	function initializeRival($connection, $destination, $rid)
	{
		$my_query = 'SELECT * FROM players WHERE PID = '.$rid;	
		$answer_sql = mysqli_query($connection, $my_query);
		$rival_data = mysqli_fetch_array($answer_sql);

		//LOAD AVATAR
		if($rival_data['AV'] == 1)
			$rival_data['AVATAR'] = '<img class = "avatar" src="data:image/jpeg;base64,'.base64_encode($rival_data['AVIMG']).'"/>';
		else
			$rival_data['AVATAR'] = 'empty';


		//LOADING ITEMS
		$answer_sql = $connection->query("SELECT * FROM items WHERE 
		id =".$rival_data['I_BOT']." OR id=".$rival_data['I_TOP']." OR id=".$rival_data['I_MID']." 
		OR id=".$rival_data['I_LEFT']." OR id=".$rival_data['I_RIGHT']."
		ORDER BY type");

		for($cnt = 1; $cnt <= 5; $cnt++)
		{
			$item_data = mysqli_fetch_assoc($answer_sql);

			$temp_item = new Item();

			$temp_item -> initialize(
			$item_data['id'],
			$item_data['name'],
			$item_data['rarity'],
			$item_data['type'],
			$item_data['gif'],
			$item_data['lvl'],
			$item_data['hp'],
			$item_data['atc'],
			$item_data['dmg'],
			$item_data['agi'],
			$item_data['arm'],
			$item_data['cost']
			);

			$temp_item -> generateLabel();

			$temp_items[$cnt] = $temp_item;
		}

		//LOADING SKILL TREE
		$answer_sql = $connection->query("SELECT * FROM skilltrees WHERE PID = ".$rid);
		$rival_skill_tree = mysqli_fetch_array($answer_sql);
		

		$rival = new Rival();

		$rival -> initializeRival(
		$rival_data['PID'],
		$rival_data['NICK'],
		$rival_data['AVATAR'],
		$rival_data['LVL'],
		$rival_data['HP'],
		$rival_data['ATC'],
		$rival_data['DMG'],
		$rival_data['AGI'],
		$rival_data['ARM'],
		$rival_skill_tree,
		$rival_data['I_TOP'],
		$rival_data['I_LEFT'],
		$rival_data['I_MID'],
		$rival_data['I_RIGHT'],
		$rival_data['I_BOT']
		);

		$rival -> calculateAttributes($temp_items);

		$_SESSION['rival'] = $rival;


		$answer_sql->free();

		if ($destination != '') 
		{
			header($destination);
		}
	}
}

?>



