<?php

include 'Initializer.php';

session_start();
	
if(!isset($_SESSION['logged_in']))
{
	header('Location: ../index.php');
	exit();
}

	
//connecting to database
require_once "db_data.php";
$connection = @new mysqli($host, $db_user, $db_password, $db_name);
	
$player = new Player();
$player = $_SESSION['player'];

$stat_type = $_POST['stat'];


//UPDATE ATTRIBUTES
switch($stat_type)
{
	case 2:
		$new_sp = $player->spare_points -1;
		if($new_sp < 0)	{ break; }
		$my_query = "UPDATE players SET SP = ".$new_sp." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
	
		$new_atc = $player->attack +1;
		$my_query = "UPDATE players SET ATC = ".$new_atc." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
		
		break;
		
	case 3:
		$new_sp = $player->spare_points -1;
		if($new_sp < 0)	{ break; }
		$my_query = "UPDATE players SET SP = ".$new_sp." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
	
		$new_dmg = $player->damage +1;
		$my_query = "UPDATE players SET DMG = ".$new_dmg." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
		
		break;
		
	case 4:
		$new_sp = $player->spare_points -1;
		if($new_sp < 0)	{ break; }
		$my_query = "UPDATE players SET SP = ".$new_sp." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
		
		$new_agi = $player->agility +1;
		$my_query = "UPDATE players SET AGI = ".$new_agi." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
		
		break;
		
	case 5:
		$new_sp = $player->spare_points -1;
		if($new_sp < 0)	{ break; }
		$my_query = "UPDATE players SET SP = ".$new_sp." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
		
		$new_arm = $player->armor +1;
		$my_query = "UPDATE players SET ARM = ".$new_arm." WHERE PID = ".$_SESSION['id'];		
		mysqli_query($connection, $my_query);
		
		break;
}
		
//(re)initialize player
$init_object = new Initializer;
$init_object -> initializePlayer($connection, 'Location: ../pages/character.php', $player->id);

$answer_sql->free();	
$connection->close();
	
?>