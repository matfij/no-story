<?php
	session_start();
	
	include 'Initializer.php';
	
	if(!isset($_POST['login']) || !isset($_POST['password']))
	{
		head('Location: ../index.php');
		exit();
	}

	//connecting to database
	require_once "db_data.php";
	$my_connection = @new mysqli($host, $db_user, $db_password, $db_name);
	
	//checking if connected
	if($my_connection->connect_errno!=0)  // NO connection
	{
		echo "Error: ".$my_connection->connect_errno;
	}
	else  // connection OK
	{
		$login = $_POST['login'];
		$password = $_POST['password'];
		
		$login = htmlentities($login, ENT_QUOTES, "UTF-8");
		
		//checking IF login & password OK
		if($answer_sql = @$my_connection->query("SELECT * FROM players WHERE NICK ='$login'"))
		{
			$user_no = $answer_sql->num_rows;
			
			if($user_no > 0)  // login OK
			{
				$user_data = $answer_sql->fetch_assoc();
				if(password_verify($password, $user_data['PASS']))
				{
					$_SESSION['logged_in'] = true;		
					$_SESSION['id'] = $user_data['PID'];
					
					$_SESSION['purchase_completed'] = false;
					unset($_SESSION['login_error']);
			
					//(re)initialize player
					$init_object = new Initializer;
					$init_object -> initializePlayer($my_connection, 'Location: ../pages/home.php', $_SESSION['id']);
	
					
					$answer_sql->free();
				}
				else  // wrong password
				{
					//error messagge
					$_SESSION['login_error'] = 'WRONG PASSWORD';
					//going back to login page
					header('Location: ../index.php');
					exit();
				}		
			}
			else  // account does not exist
			{
				//error messagge
				$_SESSION['login_error'] = 'USER NOT FOUND';
				//going back to login page
				header('Location: ../index.php');
				exit();
			}
			
		}
		
		$my_connection->close();
	}

	
?>



