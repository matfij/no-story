<?php

require_once('../php/CombatCalculator.php');
require_once('../php/Rival.php');
require_once('../php/Player.php');

session_start();

$rival = new Rival();
$rival = $_SESSION['rival'];

$player = new Player();
$player = $_SESSION['player'];

$combatCalculator = new CombatCalculator();
$combatCalculator->executeArenaCombat($player, $rival, 0);

$json_log = $_SESSION['battle_log'];
echo $json_log;

?>