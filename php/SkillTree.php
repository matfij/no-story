<?php

class SkillTree
{
	public $skills;

	function initializeSkillTree($connection)
	{
		$sql_query = 'SELECT * FROM skills';
		$sql_answer = mysqli_query($connection, $sql_query);
		$skill_no = mysqli_num_rows($sql_answer);

		for ($i = 1; $i <= $skill_no; $i++) 
		{
			$skill = mysqli_fetch_assoc($sql_answer);

			$this->skills[$i][1] = $skill['ID'];
			$this->skills[$i][2] = $skill['Power'];
			$this->skills[$i][3] = $skill['Name'];
		}
	}

}

?>