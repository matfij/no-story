﻿<?php

class Item
{
	//general
	public $id;
	public $name;
	public $rarity;
	public $type;
	public $image;

	//attributes
	public $level;
	public $health;
	public $attack;
	public $damage;
	public $agility;
	public $armor;

	//others
	public $label;
	public $shop_label;
	public $craft;
	public $cost;


	function initialize($id, $name, $rarity, $type, $image, $level, $health, $attack, $damage, $agility, $armor, $cost)
	{
		$this->id = $id;
		$this->name = $name;
		$this->rarity = $rarity;
		$this->type = $type;
		$this->image = $image;

		$this->level = $level;
		$this->health = $health;
		$this->attack = $attack;
		$this->damage = $damage;
		$this->agility = $agility;
		$this->armor = $armor;

		$this->cost = $cost;
	}


	function generateLabel()
	{
		$temp_label = 'yet empty...';

		switch($this->rarity)
		{
			case 1:
				$temp_label = '<span style="color:#9d9d9d;"><b>'.$this->name."</b></span><br/>";
				break;
			case 2:
				$temp_label = '<span style="color:#fff;"><b>'.$this->name."</b></span><br/>";
				break;
			case 3:
				$temp_label = '<span style="color:#0080ff;"><b>'.$this->name."</b></span><br/>";
				break;
			case 4:
				$temp_label = '<span style="color:#ff8000;"><b>'.$this->name."</b></span><br/>";
				break;
		}
					
		if($this->health != 0) 
			$temp_label = $temp_label." <br/>health: ".$this->health;
		if($this->attack != 0) 
			$temp_label = $temp_label." <br/>attack: ".$this->attack;
		if($this->damage != 0) 
			$temp_label = $temp_label." <br/>damage: ".$this->damage;
		if($this->agility != 0) 
			$temp_label = $temp_label." <br/>agility: ".$this->agility;
		if($this->armor != 0) 
			$temp_label = $temp_label." <br/>armor: ".$this->armor;

		if($this->level != 0)
			$temp_label = $temp_label." <br/><br/>level: ".$this->level;

		$this->label = $temp_label;
	}


	function generateShopLabel($buyer_level, $buyer_gold, $buyer_unique_shards, $buyer_legendary_shards)
	{

		switch($this->rarity)
		{
			case 1:
				$temp_label = '<span style="color:#9d9d9d;"><b>'.$this->name."</b></span><br/>";
				break;
			case 2:
				$temp_label = '<span style="color:#fff;"><b>'.$this->name."</b></span><br/>";
				break;
			case 3:
				$temp_label = '<span style="color:#0080ff;"><b>'.$this->name."</b></span><br/>";
				break;
			case 4:
				$temp_label = '<span style="color:#ff8000;"><b>'.$this->name."</b></span><br/>";
				break;
		}
					
		if($this->health != 0) 
			$temp_label = $temp_label." <br/>health: ".$this->health;
		if($this->attack != 0) 
			$temp_label = $temp_label." <br/>attack: ".$this->attack;
		if($this->damage != 0) 
			$temp_label = $temp_label." <br/>damage: ".$this->damage;
		if($this->agility != 0) 
			$temp_label = $temp_label." <br/>agility: ".$this->agility;
		if($this->armor != 0) 
			$temp_label = $temp_label." <br/>armor: ".$this->armor;
			
					
		if($buyer_level < $this->level)
			$temp_label = $temp_label.'<br/><br/>level: <span style="color:red;">'.$this->level.'</span>';
		else
			$temp_label = $temp_label."<br/><br/>level: ".$this->level;
		


		if($this->rarity == 3)
		{
			if($buyer_unique_shards < $this->cost)
				$temp_label = $temp_label.' <br/>unique shards: <span style="color:red;">'.$this->cost.'</span>';
			else
				$temp_label = $temp_label.'<br/>unique shards: '.$this->cost.'';
		}
		else if($this->rarity == 4)
		{
			if($buyer_gold < 20*$this->cost && $buyer_legendary_shards < $this->cost)
			{
				$temp_label = $temp_label.' <br/>gold: <span style="color:red;">'.(20*$this->cost).'</span>';
				$temp_label = $temp_label.' <br/>legendary shards: <span style="color:red;">'.$this->cost.'</span>';
			}
			else if($buyer_gold >= 20*$this->cost && $buyer_legendary_shards < $this->cost)
			{
				$temp_label = $temp_label.' <br/>gold: '.(20*$this->cost).'';
				$temp_label = $temp_label.' <br/>legendary shards: <span style="color:red;">'.$this->cost.'</span>';
			}
			else if($buyer_gold < 20*$this->cost && $buyer_legendary_shards >= $this->cost)
			{
				$temp_label = $temp_label.' <br/>gold: <span style="color:red;">'.(20*$this->cost).'</span>';
				$temp_label = $temp_label.' <br/>legendary shards: '.$this->cost.'';
			}
			else
			{
				$temp_label = $temp_label.' <br/>gold: '.(20*$this->cost).'';
				$temp_label = $temp_label.' <br/>legendary shards: '.$this->cost.'';
			}
		}
		else
		{
			if($buyer_gold < $this->cost)
				$temp_label = $temp_label.'<br/>price: <span style="color:red;">'.$this->cost.'</span>';
			else
				$temp_label = $temp_label."<br/>price: ".$this->cost;
		}


		$temp_label = $temp_label.' <form action = "../php/shop_purchase.php" method = "post" class = "itemForm">
					<div class = "hiddenInput"><input type = "text" name = "item_id" value='.$this->id.'></div>
					<input type = "submit" value = "PURCHASE" class = "itemBtn"/></form>';
					
		$this->shop_label = $temp_label;
	}


	function display($size)
	{
		if($this->id != 0) 
		{
			if($this->type == 1)
			{
				echo '<div class = "eqSmall">
					<img class = "itemEqS" src = "../img/items/'.$this->image.'.gif"/>
					<div class = "itemLabel itemLabelTop">'.$this->label.'</div>
				</div>';
			}
			else if($this->type == 5)
			{
				echo '<div class = "eqSmall">
					<img class = "itemEqS" src = "../img/items/'.$this->image.'.gif"/>
					<div class = "itemLabel itemLabelBot">'.$this->label.'</div>
				</div>';
			}
			else
			{
				echo '<div class = "eqLarge">
					<img class = "itemEqL" src = "../img/items/'.$this->image.'.gif"/>
					<div class = "itemLabel">'.$this->label.'</div>
				</div>';
			}
		}
		else
		{
			if($size == 1)
				echo '<div class = "eqSmall"><img class = "itemEqS" ></div>';
			else
				echo '<div class = "eqLarge"><img class = "itemEqL" ></div>';
		}
		
	}	
}

?>



